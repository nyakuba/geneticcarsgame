package com.me.geneticcars.test;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

public class OtherTests {
	@Test
	public void testFinal(){
		// arrays are not immutable
		final int a[] = new int[10];
		a[1] = 5;
		org.junit.Assert.assertEquals(5, a[1]);
		a[1] = 6;
		org.junit.Assert.assertEquals(6, a[1]);
	}
	@Test
	public void testImmutable(){
		ArrayList<Integer> list = new ArrayList<Integer>(20);
		list.add(1);
		list.add(2);
		ImmutableList.Builder<Integer> builder = new ImmutableList.Builder<>();
		builder.add(1,2,3,4,5);
		ImmutableList<Integer> imlist = builder.build();
		org.junit.Assert.assertEquals(new Integer(1), imlist.get(0));
//		org.junit.Assert.assertEquals(new Integer(0), imlist.get(3));
	}
}
