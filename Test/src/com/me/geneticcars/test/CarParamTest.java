package com.me.geneticcars.test;

import org.junit.Assert;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.me.geneticcars.Chromosome;

public class CarParamTest {
	@Test
	public void constrFromParams() {
		System.out.println(">>> Constr test:");
		// создаем объект
		int vertices_count = 3;
		Vector2 vertices[] = new Vector2[Chromosome.MAX_VERTICES_COUNT];
		for (int i = 0; i < Chromosome.MAX_VERTICES_COUNT; i++)
			vertices[i] = new Vector2();
		vertices[0].set(20, 20);
		vertices[1].set(50, -50);
		vertices[2].set(-30, 30);
		int wheel_count = 0;
		Integer wheel_size[] = new Integer[Chromosome.MAX_WHEEL_COUNT];
		Integer wheel_speed[] = new Integer[Chromosome.MAX_WHEEL_COUNT];
		Integer wheel_vertices_index[] = new Integer[Chromosome.MAX_WHEEL_COUNT];
		for (int i = 0; i < Chromosome.MAX_WHEEL_COUNT; i++) {
			wheel_size[i] = new Integer(0);
			wheel_speed[i] = new Integer(0);
			wheel_vertices_index[i] = new Integer(0);
		}
		wheel_size[Chromosome.MAX_WHEEL_COUNT - 1] = Chromosome.MAX_WHEEL_SIZE;
		wheel_speed[Chromosome.MAX_WHEEL_COUNT - 1] = Chromosome.MAX_WHEEL_SPEED;
		wheel_vertices_index[Chromosome.MAX_WHEEL_COUNT - 1] = 1;

		Chromosome param = new Chromosome(vertices_count, vertices, wheel_count,
				integerToIntArr(wheel_size), integerToIntArr(wheel_speed),
				integerToIntArr(wheel_vertices_index));

		// сравниваем значения
		Assert.assertEquals(vertices_count, param.m_vertices_count);
		for(int i=0; i<vertices_count; i++)
			Assert.assertEquals(vertices[i], param.getVector2Array()[i]);
		Assert.assertEquals(wheel_count, param.m_wheel_count);
		for(int i=0; i<wheel_count; i++){
			Assert.assertEquals(wheel_size[i], (Integer)param.m_wheel_size.toArray()[i]);
			Assert.assertEquals(wheel_speed[i], (Integer)param.m_wheel_speed.toArray()[i]);
		}
		System.out.println("<<< End const test:");
	}

	@Test
	public void mutation() {
		System.out.println(">>> Mutation test:");
		// создаем начальный объект параметров
		Vector2 vert[] = new Vector2[Chromosome.MAX_VERTICES_COUNT];
		for (int i = 0; i < vert.length; i++)
			vert[i] = new Vector2(0, 0);
		vert[0].set(-20, 0);
		vert[1].set(0, 20);
		vert[2].set(20, 0);
		int wsize[] = new int[Chromosome.MAX_WHEEL_COUNT];
		int wspeed[] = new int[Chromosome.MAX_WHEEL_COUNT];
		int wvindex[] = new int[Chromosome.MAX_WHEEL_COUNT];
		Chromosome to_mutation = new Chromosome(3, vert, 0, wsize, wspeed, wvindex);
		System.out.println(to_mutation);
		// мутируем с вероятностью 100% 10 раз. Каждые новые параметры выводим в
		// консоль.
		for (int i = 0; i < 100; i++) {
			System.out.println("-----");
			to_mutation = to_mutation.mutate(100, null);
			System.out.println(to_mutation);
		}
		System.out.println("<<< End mutation test:");
	}

	@Test
	public void crossbreeding() {
		System.out.println(">>> Crossbreeding test:");
		// инициализируем параметры для первой особи
		int vertices_count = 3;
		Vector2 vertices[] = new Vector2[Chromosome.MAX_VERTICES_COUNT];
		for (int i = 0; i < Chromosome.MAX_VERTICES_COUNT; i++)
			vertices[i] = new Vector2();
		int wheel_count = 0;
		Integer wheel_size[] = new Integer[Chromosome.MAX_WHEEL_COUNT];
		Integer wheel_speed[] = new Integer[Chromosome.MAX_WHEEL_COUNT];
		Integer wheel_vertices_index[] = new Integer[Chromosome.MAX_WHEEL_COUNT];
		for (int i = 0; i < Chromosome.MAX_WHEEL_COUNT; i++) {
			wheel_size[i] = new Integer(0);
			wheel_speed[i] = new Integer(0);
			wheel_vertices_index[i] = new Integer(0);
		}
		vertices[0].set(-20, 0);
		vertices[1].set(0, 20);
		vertices[2].set(20, 0);
		// создаем первую особь
		Chromosome param1 = new Chromosome(vertices_count, vertices, wheel_count,
				integerToIntArr(wheel_size), integerToIntArr(wheel_speed),
				integerToIntArr(wheel_vertices_index));

		vertices_count = Chromosome.MAX_VERTICES_COUNT;
		for (int i = 0; i < Chromosome.MAX_VERTICES_COUNT; i++) {
			vertices[i].set(10, -10);
		}
		wheel_count = Chromosome.MAX_WHEEL_COUNT;
		for (int i = 0; i < Chromosome.MAX_WHEEL_COUNT; i++) {
			wheel_size[i] = Chromosome.MAX_WHEEL_SIZE;
			wheel_speed[i] = Chromosome.MAX_WHEEL_SPEED;
			wheel_vertices_index[i] = 1;
		}
		// создаем вторую особь
		Chromosome param2 = new Chromosome(vertices_count, vertices, wheel_count,
				integerToIntArr(wheel_size), integerToIntArr(wheel_speed),
				integerToIntArr(wheel_vertices_index));

		// скрещиваемся
		System.out.println("param1:\n" + param1 + '\n' + "param2:\n" + param2);

		for(int i=0; i<100; i++)
			System.out.println("crossbreeding:\n"
					+ param1.toBeABadGirl(param2, null));

		System.out.println("<<< End crossbreeding test:");
	}

	private int[] integerToIntArr(Integer[] array) {
		int result[] = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			result[i] = array[i].intValue();
		}
		return result;
	}
}