package com.me.geneticcars.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.badlogic.gdx.math.Vector2;
import com.me.geneticcars.Chromosome;
import com.me.geneticcars.IOUtils;

public class IOUtilsTest {
	@Test
	public void createXMLFile() {
		System.out.println(">>> Create XML test:");
		// инициализируем параметры Chromosome
		int vertices_count = 3;
		Vector2 vertices[] = new Vector2[Chromosome.MAX_VERTICES_COUNT];
		for (int i = 0; i < Chromosome.MAX_VERTICES_COUNT; i++)
			vertices[i] = new Vector2();
		int wheel_count = 3;
		int wheel_size[] = new int[Chromosome.MAX_WHEEL_COUNT];
		int wheel_speed[] = new int[Chromosome.MAX_WHEEL_COUNT];
		int wheel_vertices_index[] = new int[Chromosome.MAX_WHEEL_COUNT];
		for (int i = 0; i < Chromosome.MAX_WHEEL_COUNT; i++) {
			wheel_size[i] = (i+1)*10;
			wheel_speed[i] = 2*i;
			wheel_vertices_index[i] = i;
		}
		vertices[0].set(-20, 0);
		vertices[1].set(0, 20);
		vertices[2].set(20, 0);
		// создаем первую особь
		Chromosome param1 = new Chromosome(vertices_count, vertices,
				wheel_count, wheel_size, wheel_speed, wheel_vertices_index);
		System.out.println(param1);
		IOUtils xml = new IOUtils("test_output.xml");
		xml.create();
		xml.addToXML(param1);
		xml.addToXML(new Vector2(111,222));
		xml.addToXML(param1);
		xml.addToXML(new Vector2(555,666));
		xml.close();
		System.out.println("<<< End create XML test:");
	}
	@Test
	public void readXMLFile(){
		System.out.println(">>> Read from XML test:");
		IOUtils xml = new IOUtils("test_output.xml");
		List<Chromosome> l = null;
		try {
			l = xml.getChromosomes();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("list size = "+l.size());
		for(Chromosome chr : l){
			System.out.println(chr.toString());
		}
		
		try {
			List<Vector2> pos = xml.getPositions();
			System.out.println(pos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(">>> End read from XML test:");
	}
	@Test
	public void getXMLError(){
		System.out.println(">>> Read XML error test:");
		IOUtils xml = new IOUtils("not_exist");
		try {
			xml.getChromosomes();
		} catch(Exception e){
			Assert.assertTrue(e instanceof FileNotFoundException);
			e.printStackTrace();
		}
		
		// closing non-opened file
		System.out.println("closing closed file");
		xml.close();
		System.out.println("done");
		
		//double creating
		IOUtils xml1 = new IOUtils("test_output_creation.xml");
		xml1.create();
		xml1.create();
		xml1.close();
		System.out.println(">>> End read XML error test:");
	}
}
