package com.me.geneticcars;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;


/**
 * Implements array of Panels, that can be used to form ground and other static things.
 */
public class PanelSet {
	
	static final float DEFAULT_HX = 100f;
	static final float DEFAULT_HY = 10f;
	static final Vector2 DEFAULT_POSITION = new Vector2(0f, 0f);
	static final float DEFAULT_ANGLE = 0f;
	static final int MAX_ANGLE_DIFF = 50;
	static final int LEVEL1 = 50;
	static final int LEVEL2 = 60;
	static final int LEVEL3 = 60;
	static final int LEVEL4 = 70;
	static final int LEVEL5 = 80;
	
	public PanelSet(World world){
		m_panelSet = new ArrayList<Panel>();
		m_world = world;
	}
	
	public final int size(){
		return m_panelSet.size();
	}
	
	/// add panel in given position
	/// @param position - position
	/// @param hx - horizontal length
	/// @param hy - vertical length
	/// @
	public void add(Vector2 position, float hx, float hy, float angle){
		m_panelSet.add(new Panel(m_world, position, hx, hy, (float) (angle * MathUtils.degreesToRadians)));
	}
	
	public void add(float hx, float hy, float angle){
		if(m_panelSet.isEmpty()){
			m_panelSet.add(new Panel(m_world, DEFAULT_POSITION, hx, hy, (float) (angle * MathUtils.degreesToRadians)));
		} else {
			m_panelSet.add(new Panel(m_world, m_panelSet.get(m_panelSet.size()-1).m_end, hx, hy,
					(float) (angle * Math.PI / 180)));
		}
	}
	
	public void addRandomPanel(){
		if(m_panelSet.isEmpty()){
			m_panelSet.add(new Panel(m_world, DEFAULT_POSITION, DEFAULT_HX, DEFAULT_HY, (float) (DEFAULT_ANGLE * Math.PI / 180)));
		} else {
			Random diff = new Random();
			float angle = (m_panelSet.get(m_panelSet.size()-1).m_angle) + diff.nextInt()%MAX_ANGLE_DIFF;
			angle *= Math.PI / 180;
			m_panelSet.add(new Panel(m_world, m_panelSet.get(m_panelSet.size()-1).m_end, DEFAULT_HX, DEFAULT_HY, angle));
		}
	}
	
	public void generateRandomPanel(int panel_count, int seed, Random gen){
		if(gen == null)
			gen = new Random(seed);
		else 
			gen.setSeed(seed);
		// TODO: fix hardcoding
		if(panel_count >= 5){
			int level_panel_count = panel_count/5;
			for(int i=0; i<level_panel_count; i++){
				float angle = gen.nextInt(LEVEL1*2)-LEVEL1;
				m_panelSet.add(new Panel(m_world, m_panelSet.get(m_panelSet.size()-1).m_end, DEFAULT_HX, DEFAULT_HY, angle*MathUtils.degreesToRadians));
			}
			for(int i=0; i<level_panel_count; i++){
				float angle = gen.nextInt(LEVEL2*2)-LEVEL2;
				m_panelSet.add(new Panel(m_world, m_panelSet.get(m_panelSet.size()-1).m_end, DEFAULT_HX, DEFAULT_HY, angle*MathUtils.degreesToRadians));
			}
			for(int i=0; i<level_panel_count; i++){
				float angle = gen.nextInt(LEVEL3*2)-LEVEL3;
				m_panelSet.add(new Panel(m_world, m_panelSet.get(m_panelSet.size()-1).m_end, DEFAULT_HX, DEFAULT_HY, angle*MathUtils.degreesToRadians));
			}
			for(int i=0; i<level_panel_count; i++){
				float angle = gen.nextInt(LEVEL4*2)-LEVEL4;
				m_panelSet.add(new Panel(m_world, m_panelSet.get(m_panelSet.size()-1).m_end, DEFAULT_HX, DEFAULT_HY, angle*MathUtils.degreesToRadians));
			}
			for(int i=level_panel_count*4; i<panel_count; i++){
				float angle = gen.nextInt(LEVEL5*2)-LEVEL5;
				m_panelSet.add(new Panel(m_world, m_panelSet.get(m_panelSet.size()-1).m_end, DEFAULT_HX, DEFAULT_HY, angle*MathUtils.degreesToRadians));
			}
		} else {
			for(int i=0; i<panel_count; i++){
				float angle = gen.nextInt(LEVEL5*2)-LEVEL5;
				m_panelSet.add(new Panel(m_world, m_panelSet.get(m_panelSet.size()-1).m_end, DEFAULT_HX, DEFAULT_HY, angle*MathUtils.degreesToRadians));
			}
		}
	}
	
	public void dispose() {
		for(Panel panel : m_panelSet) {
			panel.dispose();
		}
	}
	
	private ArrayList<Panel> m_panelSet;
	private World m_world;
}
