package com.me.geneticcars;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;


public class CarGenerationView {
	
	/*ArrayList<Car> m_carGen = new ArrayList<Car>();
	ArrayList<counterSteps> m_cnt = new ArrayList<counterSteps>();
	ArrayList<Float> m_carCriterion = new ArrayList<Float>();*/
	
	List<CarPassport> m_carPas = new ArrayList<CarPassport>();
	List<CarPassport> cemetery = new ArrayList<CarPassport>();
	
	//static final int CAR_NUMBER = 21;
	static final Vector2 START_POSITION = new Vector2(100, 200);
	//Количество шагов для проверки на мертвость
	static final int STEPS_COUNT = 4;
	//Минимальное отклонение при расчете мертвости
	static final float DISTANCE_RATE = 150;
	//счетчик тиков.
	long m_count;
	//количество тиков между проверками положения
	static final int TICS_CONTROL = 150;
	
	World m_world;
	
	
	CarGenerationView(World world){
		m_world = world;	
	}
	
	public void inputPopulation(List<Chromosome> newParam){
		//m_carPas.clear();
		for(int i = 0; i < newParam.size(); i++){
			
			//CarParameters param = CarGenerationView.convert(newParam.get(i));
			CarParameters param = new CarParameters(newParam.get(i));
			
			//System.out.println(newParam.size());
			CarPassport newCarPas = new CarPassport();
			Car newCar = null;

			try{
				
				newCar = new Car(param);
				newCar.create(m_world);
				
			} catch (Exception ex){
				System.err.println(ex);
			}
		
			counterSteps cnt = new counterSteps();
			
			newCarPas.c_car = newCar;
			newCarPas.c_cnt = cnt;
			newCarPas.c_cromo = newParam.get(i);
			
			m_carPas.add(newCarPas);
			//m_cnt.add(cnt);	
			//m_carGen.add(newCar);
			//System.out.println(m_carPas.size());
		}
		
		return;
	}
	
	/**
	 * Возвращает ссылку на машинку-лидера.
	 * Лидер - тот кто имеет значение getPosition().x
	 * больше остальных(т.е. находится дальше(правее) всех)
	 * @return
	 */
	public Car getLeader(){
		Car leader = m_carPas.get(0).c_car;

		for(int i = 1; i < m_carPas.size(); i++){
			if(m_carPas.get(i).c_car.getPosition().x > leader.getPosition().x){
				leader = m_carPas.get(i).c_car;
			}
		}
		
		return leader;
	}
	
	/**
	 * инкрементирует счетчик времени и вызывает методы проверки на смерть машинки
	 * проверяет наличие живых машинок, в случае их отсутствия вызывает функцию подсчета 
	 * приспособленности и возвращает результат. 
	 */
	public ArrayList<Chromosome> renderCounter(){
		m_count++;

		
		if(m_count%TICS_CONTROL == 0){
			for(int i = 0; i < m_carPas.size(); i++){
				if(!m_carPas.get(i).c_car.m_imDie){
					if(m_carPas.get(i).c_cnt.newStep(m_carPas.get(i).c_car.getPosition().x)){
						criterionFunct();
						m_carPas.get(i).c_car.mustDie();	
						cemetery.add(m_carPas.get(i));
						
						m_carPas.get(i).c_car.dispose();
						m_carPas.remove(i);
					}
				}
			}
			
		}
		
		// TODO Да, это даже не костыль - это больше похоже на инвалидное кресло
		//Но фича написана в день релиза и работает - посему рефакторингу не подлежит
		if(cemetery.size() >= 3){
			
			ArrayList<Chromosome> catafalque = new ArrayList<Chromosome>();
		

				int mas[] = new int[3];
				mas[0] = cemetery.size()-3;
				mas[1] = cemetery.size()-2;
				mas[2] = cemetery.size()-1;
				
				if(cemetery.get(cemetery.size()-2).c_criterion < 
						cemetery.get(cemetery.size()-3).c_criterion){
					int temp = mas[0];
					mas[0] = mas[1];
					mas[1] = temp;
				}
				for(int j=1; j>=0; j--){
					if(cemetery.get(cemetery.size()-3+j).c_criterion
							< cemetery.get(cemetery.size()-1).c_criterion){
						int temp = mas[j+1];
						mas[j+1] = mas[2];
						mas[2] = temp;
						break;
					}
				}
				
				catafalque.add(cemetery.get(mas[0]).c_cromo);
				catafalque.add(cemetery.get(mas[1]).c_cromo);
				catafalque.add(cemetery.get(mas[2]).c_cromo);
				
				cemetery.remove(cemetery.size() - 1);
				cemetery.remove(cemetery.size() - 1);
				cemetery.remove(cemetery.size() - 1);
			
			return catafalque;
		}
	
		
		
		return null;
	}
	
	/**
	 * Вычисляет функцию приспособленности и заносит её
	 * в поле c_criterion
	 */
	public void criterionFunct(){
		
		for(int i = 0; i < m_carPas.size(); i++){
			m_carPas.get(i).c_criterion = m_carPas.get(i).c_car.getPosition().x;
		}
		
		return;
	}
	
	/**!!!!!!!!!!!!!!Атавизм
	 * Проверяет наличие живых машинок в поколении
	 * 
	 * @return
	 * true - если есть хоть одна живая машинка
	 */
	public boolean somebodyLive(){
		for(int i = 0; i < m_carPas.size(); i++){
			if(m_carPas.get(i).c_car.m_imDie == false)
				return true;
		}
		return false;	
	}
	
	
	public class counterSteps{
		private float count[] = new float [STEPS_COUNT];
		int i;
		
		public counterSteps(){
			for(int j = 0; j < STEPS_COUNT; j++)
				count[j] = j * 300;
		}
		
		public boolean newStep(float x){
			count[i] = x;
			i = (i + 1)%STEPS_COUNT;
			
			int j = 0;
			
			while(j < STEPS_COUNT){
				
				if((count[(j+1)%STEPS_COUNT] - count[j]) > DISTANCE_RATE)
					return false;
				
				j = j + 1;
			}
			
			return true;
		}
	};
	
	/**
	 * Класс связывает вместе машинку, счетчик тиков для машинки, 
	 * критерий приспособленности и хромосомы, по которым машина была построена.
	 * @author alexander
	 *
	 */
	public class CarPassport{
		
		public Car c_car;
		public counterSteps c_cnt;
		public float c_criterion;
		public Chromosome c_cromo;
		
		public CarPassport(){
			c_car = null;
			c_cnt = null;
			c_criterion = 0;
		}
		
	};

}
