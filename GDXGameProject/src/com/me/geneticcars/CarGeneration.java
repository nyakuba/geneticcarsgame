package com.me.geneticcars;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class CarGeneration {
	
	public static final int MUTATIONS_COUNT = 2;
	public final int CAR_NUMBER;
	public List<Chromosome> populationChromo;
	public ArrayList<Chromosome> carForCross;
	
	float result[];
	CarGenerationView CGV;
	World m_world;
	Random m_gen;
	public final int m_mutation_rate;

	CarGeneration(World world, Random gen, int mutation_rate, int car_number){
		m_mutation_rate = mutation_rate;
		car_number+=3-car_number%3;
		CAR_NUMBER = car_number;
		m_world = world;
		m_gen = gen;
		
		populationChromo = makeRandomPopulation(CAR_NUMBER);
		CGV = new CarGenerationView(world);
	}
	
	CarGeneration(World world, Random gen, int mutation_rate, List<Chromosome> init_population){
		int car_number = init_population.size();
		car_number-=car_number%3;
		CAR_NUMBER = car_number;
		m_mutation_rate = mutation_rate;
		m_world = world;
		m_gen = gen;
		
		populationChromo = init_population;
		CGV = new CarGenerationView(world);
	}
	
	public void initSelection(){
		
		CGV.inputPopulation(populationChromo);
		//CGV.inputPopulation(populationParameters);
	}
	
	public void selection(){
		//result = CGV.renderCounter();
		
		carForCross = CGV.renderCounter();
		
		if(carForCross == null){
			
		}else{
			
			CGV.inputPopulation(tournamentGenetic(carForCross));
		}
		
	}
	private ArrayList<Chromosome> tournamentGenetic(ArrayList<Chromosome> cars){
		ArrayList<Chromosome> newCars = new ArrayList<Chromosome>();
		
		Chromosome best = cars.get(2);
		Chromosome middle = cars.get(1);
		Chromosome loser = cars.get(0);
		
		loser = best.toBeABadGirl(middle, m_gen);
		for(int i=0; i<MUTATIONS_COUNT; i++)
			loser = loser.mutate(m_mutation_rate, m_gen);
		
		newCars.add(loser);
		newCars.add(middle);
		newCars.add(best);
		
		return newCars;	
	}
	
	
	private List<Chromosome> makeRandomPopulation(int size){
		List<Chromosome> listParam = new ArrayList<Chromosome>(CAR_NUMBER);
		//TODO Коля не любит тонкие формы. 
		for(int k = 0; k < size; k++){
			Random gen = new Random();
			int vertCount = Chromosome.MIN_VERTICES_COUNT + Math.abs(gen.nextInt(Chromosome.MAX_VERTICES_COUNT-Chromosome.MIN_VERTICES_COUNT));
			int wheelCount = Math.abs(gen.nextInt(2)); //initCount
			
			Vector2 vert[] = new Vector2[vertCount];
			vert[0] = new Vector2(gen.nextInt(Chromosome.MAX_VERTICE_SIZE),gen.nextInt(Chromosome.MAX_VERTICE_SIZE));
			vert[1] = new Vector2(gen.nextInt(Chromosome.MAX_VERTICE_SIZE),gen.nextInt(Chromosome.MAX_VERTICE_SIZE));
			vert[2] = new Vector2(gen.nextInt(Chromosome.MAX_VERTICE_SIZE),gen.nextInt(Chromosome.MAX_VERTICE_SIZE));
			for(int j = 3; j < vertCount; j++){	
				vert[j] = new Vector2(gen.nextInt(Chromosome.MAX_VERTICE_SIZE),gen.nextInt(Chromosome.MAX_VERTICE_SIZE));
			}
			
			int radius[] = new int[wheelCount];
			int speed[] = new int[wheelCount];	
			int wheelIndex[] = new int[wheelCount];
			
			for(int j = 0; j < wheelCount; j++){	
				radius[j] = gen.nextInt(Chromosome.MAX_WHEEL_SIZE);
				if(radius[j]<Chromosome.MIN_WHEEL_SIZE)
					radius[j] = Chromosome.MIN_WHEEL_SIZE;
				speed[j] = gen.nextInt(Chromosome.MAX_WHEEL_SPEED);
				wheelIndex[j] = j;
			}
						
			Chromosome cparam = new Chromosome(vertCount, vert, wheelCount, radius, speed, wheelIndex);
			System.out.println(cparam.toString());
			listParam.add(cparam);
		}
		
		return listParam;
	}
	
	
}

