package com.me.geneticcars;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Class implements panel, that have start, end point and orientation.
 * It can be used to form relief of the ground.
 */
public class Panel {

	static final float PANEL_DENSITY = 0.0f;
	
	public final Vector2 m_position;
	public final Vector2 m_start;
	public final Vector2 m_end;
	public final float m_angle;

	/// Creates panel.
	/// @param world - our world
	/// @param position - position of Panel
	/// @param hx - horizontal length
	/// @param hy - vertical length
	/// @param angle - rotation angle
	public Panel(World world, Vector2 position, float hx, float hy, float angle) {
		m_position = new Vector2(position.x, position.y);
		m_angle = angle;
		m_start = new Vector2(position);
		Vector2 relStartCoord = new Vector2((float) (hx * Math.cos(m_angle)),
				(float) (hx * Math.sin(m_angle)));
		m_end = new Vector2(position.add(relStartCoord.x * 2,
				relStartCoord.y * 2));

		//create body
		m_def = new BodyDef();
		m_def.position.set(m_position);
		m_body = world.createBody(m_def);
		m_shape = new PolygonShape();
		m_shape.setAsBox(hx, hy, relStartCoord, m_angle);
		m_body.createFixture(m_shape, PANEL_DENSITY);
	}
	
	public void dispose() {
		m_shape.dispose();
	}
	
	//private fields
	private BodyDef m_def;
	private Body m_body;
	private PolygonShape m_shape;
}
