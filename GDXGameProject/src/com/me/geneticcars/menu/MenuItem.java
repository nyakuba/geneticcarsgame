package com.me.geneticcars.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class MenuItem extends Actor {
	
	private TextureRegion m_region;
	
	public MenuItem(String filename) {
		m_region = new TextureRegion(new Texture(Gdx.files.internal(filename)));
		super.setBounds(0, 0, m_region.getRegionWidth(), m_region.getRegionHeight());
		super.setOrigin(m_region.getRegionWidth()/2, m_region.getRegionHeight()/2);
		super.setScale(1, 1);
		super.setColor(1,1,1,1);
		super.setRotation(0);
		setColor(0.7f, 0.7f, 0.7f, 1f);
	}
	
	@Override
    public void draw (SpriteBatch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(m_region, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
    }
}
