package com.me.geneticcars.menu.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.me.geneticcars.ScreenChange;
import com.me.geneticcars.menu.InitParams;
import com.me.geneticcars.menu.MenuWorld;
import com.me.geneticcars.menu.controller.MenuInputActions;
import com.me.geneticcars.menu.view.MainMenuActors;
import com.me.geneticcars.menu.view.MainMenuScreen;
import com.me.geneticcars.menu.view.OptionsMenuActors;

public class Menu implements MenuInputActions {

	private ScreenChange game;
	private Stage stage;
	private BitmapFont font;

	private MainMenuActors m_mainMenu;
	private OptionsMenuActors m_optionsMenu;
	
	public Menu(ScreenChange game, Stage st, MenuWorld world) {

		this.game = game;

		stage = st;
		stage.setViewport(MainMenuScreen.X_RESOLUTION, MainMenuScreen.Y_RESOLUTION, true);
		
		font = new BitmapFont(Gdx.files.internal("my_font.fnt"), false);
		font.setColor(1f, 0f, 0f, 1f);


		m_mainMenu = new MainMenuActors(this);
		m_mainMenu.addToStage(stage);

		m_optionsMenu = new OptionsMenuActors(this, font);
		m_optionsMenu.addToStage(stage);

		setMode(Mode.MAIN);
	}

	public InitParams getParams(){
		// starting params
		return m_optionsMenu.getParams();
	}
	
	public void setGameScreen() {
		game.setGameScreen();
	}

	public void setMode(Mode mode) {
		switch (mode) {
		case MAIN:
			m_optionsMenu.hide();
			m_mainMenu.show();
			break;
		case OPTIONS:
			m_mainMenu.hide();
			m_optionsMenu.show();
			break;
		case DISABLE:
			m_mainMenu.hide();
			m_optionsMenu.hide();
		}
	}

	public void dispose(){
		font.dispose();
		m_mainMenu.dispose();
		m_optionsMenu.dispose();
	}
	
	public void exitGame() {
		Gdx.app.exit();
	}

	@Override
	public void loadAndSetGameScreen() {
		game.loadAndSetGameScreen();		
	}
}
