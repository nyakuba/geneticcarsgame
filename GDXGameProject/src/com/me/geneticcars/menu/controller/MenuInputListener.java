package com.me.geneticcars.menu.controller;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class MenuInputListener extends InputListener {
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer,
			int button) {
		Actor listenerActor = event.getListenerActor();
		listenerActor.setColor(1f, 1f, 1f, 1);
		return true;
	}
	
	@Override
	public void enter(InputEvent event, float x, float y, int pointer,
			Actor fromActor) {
		// TODO Auto-generated method stub
		Actor listenerActor = event.getListenerActor();
		listenerActor.setColor(1f, 1f, 1f, 1);
		super.enter(event, x, y, pointer, fromActor);
	}
	
	@Override
	public void exit(InputEvent event, float x, float y, int pointer,
			Actor toActor) {
		// TODO Auto-generated method stub
		Actor listenerActor = event.getListenerActor();
		listenerActor.setColor(0.7f, 0.7f, 0.7f, 1f);
		super.exit(event, x, y, pointer, toActor);
	}
}
