package com.me.geneticcars.menu.controller;

public interface MenuInputActions {
	public enum Mode {
		MAIN, OPTIONS, DISABLE;
	}
	public void setGameScreen();
	public void loadAndSetGameScreen();
	public void setMode(Mode mode);
	public void exitGame();
}
