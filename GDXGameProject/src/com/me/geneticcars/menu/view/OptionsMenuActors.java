package com.me.geneticcars.menu.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Peripheral;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.me.geneticcars.menu.InitParams;
import com.me.geneticcars.menu.controller.MenuInputListener;
import com.me.geneticcars.menu.model.Menu;

public class OptionsMenuActors {

	private final String DEFAULT_WHEEL_COUNT = "5";
	private final String DEFAULT_MUTATION_RATE = "30";
	private final String DEFAULT_CAR_COUNT = "21";
	private final String DEFAULT_EDGES_COUNT = "10";
	private final String DEFAULT_SEED = "seed";
	private final String DEFAULT_PANEL_COUNT = "100";
	
	private Menu m_menu;
	private BitmapFont m_font;
	private Skin m_optionsSkin;
	private Table table;
	private Button m_back;
	
	// params
	private TextField wheel_count;
	private TextField mutation_rate;
	private TextField car_count;
	private TextField edges_count;
	private TextField seed;
	private TextField panel_count;
	
	public OptionsMenuActors(Menu menu, BitmapFont font) {
		m_menu = menu;
		m_font = font;
		
		TextureAtlas atlas = new TextureAtlas("options_menu.pack");
		m_optionsSkin = new Skin(atlas);

		// TODO: may replace on texture region or other
		Button m_set_car_count = new Button(m_optionsSkin.getDrawable("car_count"));
		Button m_set_mutation_rate = new Button(m_optionsSkin.getDrawable("mutation_rate"));
		Button m_set_wheel_count = new Button(m_optionsSkin.getDrawable("wheel_count"));
		Button m_set_edges_count = new Button(m_optionsSkin.getDrawable("edges_count"));
		Button m_set_seed = new Button(m_optionsSkin.getDrawable("seed"));
		Button m_set_panel_count = new Button(m_optionsSkin.getDrawable("panel_count"));
		
		// back button
		m_back = new Button(m_optionsSkin.getDrawable("back"));
		m_back.setName("back");
		m_back.setPosition(50, 50);
		m_back.setPosition((MainMenuScreen.X_RESOLUTION - m_back.getWidth()) / 2, 20);
		m_back.setName("back");
		m_back.setColor(0.7f, 0.7f, 0.7f, 1);

		// common text field style
		// TODO: make beautiful style
		TextFieldStyle style = new TextFieldStyle();
		style.font = m_font;
		style.font.setScale(1);
		style.fontColor = new Color(1,1,1,1);
		style.background = m_optionsSkin.getDrawable("empty");
		style.selection = m_optionsSkin.getDrawable("fill");
		
		// text fields
		wheel_count = new TextField(DEFAULT_WHEEL_COUNT, style);
		mutation_rate = new TextField(DEFAULT_MUTATION_RATE, style);
		car_count = new TextField(DEFAULT_CAR_COUNT, style);
		edges_count = new TextField(DEFAULT_EDGES_COUNT, style);
		seed = new TextField(DEFAULT_SEED, style);
		panel_count = new TextField(DEFAULT_PANEL_COUNT, style);

		// initializing params
		mutation_rate.setMaxLength(3);
		wheel_count.setMaxLength(3);
		car_count.setMaxLength(3);
		edges_count.setMaxLength(3);
		panel_count.setMaxLength(4);
		// TODO: set beautiful margin
//		mutation_rate.setRightAligned(true);
//		wheel_count.setRightAligned(true);
//		car_count.setRightAligned(true);
		//color
		mutation_rate.setColor(0.7f, 0.7f, 0.7f, 1);
		wheel_count.setColor(0.7f, 0.7f, 0.7f, 1);
		car_count.setColor(0.7f, 0.7f, 0.7f, 1);
		edges_count.setColor(0.7f, 0.7f, 0.7f, 1);
		seed.setColor(0.7f, 0.7f, 0.7f, 1);
		panel_count.setColor(0.7f, 0.7f, 0.7f, 1);
		//name
		wheel_count.setName("wheel_count");
		mutation_rate.setName("mutation_rate");
		car_count.setName("car_count");
		edges_count.setName("edges_count");
		seed.setName("seed");
		panel_count.setName("panel_count");
		
		// text filter allows to input only numbers
		TextField.TextFieldFilter filter = new TextField.TextFieldFilter() {
			@Override
			public boolean acceptChar(TextField textField, char key) {
				// TODO rewrite
				switch(key){
				case '0':case '1':case '2':case '3':case '4':
				case '5':case '6':case '7':case '8':case '9':
					return true;
				default:
					return false;
				}
			}
		};
		
		// setting up text filter
		wheel_count.setTextFieldFilter(filter);
		mutation_rate.setTextFieldFilter(filter);
		car_count.setTextFieldFilter(filter);
		edges_count.setTextFieldFilter(filter);
		panel_count.setTextFieldFilter(filter);
		
		// creating table layout
		table = new Table(m_optionsSkin);
		Table wct = new Table(m_optionsSkin);
		Table mrt = new Table(m_optionsSkin);
		Table cct = new Table(m_optionsSkin);
		Table ect = new Table(m_optionsSkin);
		Table pct = new Table(m_optionsSkin);
		Table seedt = new Table(m_optionsSkin);
		
		// containers for text fields
		mrt.add(mutation_rate);
		wct.add(wheel_count);
		cct.add(car_count);
		ect.add(edges_count);
		pct.add(panel_count);
		seedt.add(seed);
//		mrt.setBackground(m_optionsSkin.getDrawable("car_count"));
		
		// padding
		mrt.pad(10, 10, 10, 10);
		wct.pad(10, 10, 10, 10);
		cct.pad(10, 10, 10, 10);
		ect.pad(10, 10, 10, 10);
		pct.pad(10, 10, 10, 10);
		seedt.pad(10, 10, 10, 10);
		
		// layout
		table.add(m_set_wheel_count);
		table.add(wct);
		// starting new row
		table.row();
		table.add(m_set_mutation_rate);
		table.add(mrt);
		table.row();
		table.add(m_set_car_count);
		table.add(cct);
		table.row();
		table.add(m_set_edges_count);
		table.add(ect);
		table.row();
		table.add(m_set_seed);
		table.add(seedt);
		table.row();
		table.add(m_set_panel_count);
		table.add(pct);
		
		table.setPosition(MainMenuScreen.X_RESOLUTION/2, MainMenuScreen.Y_RESOLUTION*3/4);

		// input listeners
		addListeners();
	}

	private void addListener(final TextField field, final String title){
		field.addListener(new MenuInputListener(){
			@Override
			public void touchUp(InputEvent event, float x, float y,
					int pointer, int button) {
				if(Gdx.input.isPeripheralAvailable(Peripheral.OnscreenKeyboard)){
					Gdx.input.getTextInput(new Input.TextInputListener() {
						@Override
						public void input(String text) {
							// text filter works
							field.setText(text);
						}
						@Override
						public void canceled() {
							// nothing
						}
					}, title, field.getText());
				}
				super.touchUp(event, x, y, pointer, button);
			}
		});
	}
	
	private void addListeners() {
		addListener(wheel_count, "wheel_count");
		addListener(car_count, "car_count");
		addListener(edges_count, "edges_count");
		addListener(mutation_rate, "mutation_rate");
		addListener(seed, "seed");
		addListener(panel_count, "panel_count");
		m_back.addListener(new MenuInputListener(){
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer,
					int button) {
				m_menu.setMode(Menu.Mode.MAIN);
			}
		});
	}

	public void addToStage(Stage stage) {
		stage.addActor(m_back);
		stage.addActor(table);
	}

	public void hide() {
		table.setVisible(false);
		m_back.setVisible(false);
	}

	public void show() {
		table.setVisible(true);
		m_back.setVisible(true);
	}

	public InitParams getParams() {
		int wc = Integer.valueOf(wheel_count.getText());
		int mr = Integer.valueOf(mutation_rate.getText());
		int cc = Integer.valueOf(car_count.getText());
		int ec = Integer.valueOf(edges_count.getText());
		int pc = Integer.valueOf(panel_count.getText());
		
		String s = seed.getText();
		int sd = 17;
		for(int i=0; i<s.length(); i++)
			sd = 31*sd + s.charAt(i);
		
		return new InitParams(wc, mr, cc, ec, pc, sd);
	}
	
	public void dispose(){
		m_optionsSkin.dispose();
	}
}