package com.me.geneticcars.menu.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.me.geneticcars.GeneticCars;
import com.me.geneticcars.menu.InitParams;
import com.me.geneticcars.menu.MenuWorld;
import com.me.geneticcars.menu.model.Menu;

//Implements an interactive menu screen with simple track and two cars.
public class MainMenuScreen implements Screen {

	public static final int X_RESOLUTION = 1280;
	public static final int Y_RESOLUTION = 720;

	private GeneticCars m_game;
	private Stage m_stage;
	private MenuWorld m_world;
	private Menu m_menu;
	private boolean isActive;

	public MainMenuScreen(GeneticCars game) {
		m_game = game;
		isActive = false;
	}

	public InitParams getParams(){
		// TODO: мне не нравится, что за параметрами приходится лезть очень глубоко
		// и определять кучу лишних методов.
		return m_menu.getParams();
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		m_stage.act(Gdx.graphics.getDeltaTime());
		m_stage.draw();

		m_world.render();
	}

	@Override
	public void dispose() {
		m_stage.dispose();
		m_menu.dispose();
	}

	@Override
	public void resize(int width, int height) {
		// TODO : bad
//		m_stage.setViewport(width, height, true);
	}

	@Override
	public void hide() {
		// Нужно реализовать эту функцию и show, если мы планируем возвращаться
		// в меню из игры.
		if(isActive){
			m_stage.dispose();
			m_menu.dispose();
			m_world.dispose();
			m_stage = null;
			m_world = null;
			m_menu = null;
			isActive = false;
		}
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		if(!isActive){
			m_stage = new Stage();
			m_world = new MenuWorld();
			m_menu = new Menu(m_game, m_stage, m_world);
			Gdx.input.setInputProcessor(m_stage);
			isActive = true;
		}
	}
}
