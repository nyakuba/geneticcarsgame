package com.me.geneticcars.menu.view;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.me.geneticcars.menu.MenuItem;
import com.me.geneticcars.menu.controller.MenuInputActions;
import com.me.geneticcars.menu.controller.MenuInputListener;
import com.me.geneticcars.menu.model.Menu;

public class MainMenuActors {
	public static final float HEADER_X = 512;
	public static final float HEADER_Y = 128;
	
	private Button m_new_game;
	private Button m_continue;
	private MenuItem m_options;
	private Button m_exit;
	
	private Skin m_mainmenuSkin;
	
	private MenuInputActions m_menu;
	
	public MainMenuActors(MenuInputActions menu){
		m_menu = menu;
		
		TextureAtlas atlas = new TextureAtlas("main_menu.pack");
		m_mainmenuSkin = new Skin(atlas);
		
		m_new_game = new Button(m_mainmenuSkin.getDrawable("new_game"));
		m_continue = new Button(m_mainmenuSkin.getDrawable("continue"));
		m_options = new MenuItem("options.png");
		m_exit = new Button(m_mainmenuSkin.getDrawable("exit"));
		
		m_new_game.setPosition((MainMenuScreen.X_RESOLUTION - m_new_game.getWidth()) / 10,
				MainMenuScreen.Y_RESOLUTION - m_new_game.getHeight() - 20);
		m_continue.setPosition((MainMenuScreen.X_RESOLUTION - m_continue.getWidth()) * 9 / 10,
				MainMenuScreen.Y_RESOLUTION - m_continue.getHeight() - 20);
		m_options.setPosition((MainMenuScreen.X_RESOLUTION - HEADER_X) / 2,
				MainMenuScreen.Y_RESOLUTION - 2 * HEADER_Y - 50);
		m_exit.setPosition((MainMenuScreen.X_RESOLUTION - HEADER_X / 2) / 2, 20);
		
		m_options.setName("options");
		m_new_game.setName("new_game");
		m_continue.setName("continue");
		m_exit.setName("exit");
		
		m_options.setColor(0.7f, 0.7f, 0.7f, 1);
		m_new_game.setColor(0.7f, 0.7f, 0.7f, 1);
		m_continue.setColor(0.7f, 0.7f, 0.7f, 1);
		m_exit.setColor(0.7f, 0.7f, 0.7f, 1);
		
		m_options.addAction(new Action() {
			private float total_delta;
			private boolean isGoingUp;
			{
				total_delta = new Random(0).nextInt(4) - 2;
				isGoingUp = true;
			}
			// animation
			@Override
			public boolean act(float delta) {
				// TODO Auto-generated method stub
				if(isGoingUp)
					total_delta+=delta*2;
				else
					total_delta-=delta*2;
				
				this.getActor().setRotation(total_delta);
				if(total_delta > 2){
					isGoingUp = false;
				}
				if(total_delta < -2){
					isGoingUp = true;
				}
				return false;
			}
		});
		
		addListeners();
	}
	
	private void addListeners(){
		m_new_game.addListener(new MenuInputListener(){
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer,
					int button) {
				m_menu.setGameScreen();
			}
		});
		m_continue.addListener(new MenuInputListener(){
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer,
					int button) {
				m_menu.loadAndSetGameScreen();
			}
		});
		m_options.addListener(new MenuInputListener(){
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer,
					int button) {
				m_menu.setMode(Menu.Mode.OPTIONS);
			}
		});
		m_exit.addListener(new MenuInputListener(){
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer,
					int button) {
				m_menu.exitGame();
			}
		});
	}
	
	public void addToStage(Stage stage){
		stage.addActor(m_new_game);
		stage.addActor(m_continue);
		stage.addActor(m_options);
		stage.addActor(m_exit);
	}
	
	public void hide(){
		m_new_game.setVisible(false);
		m_continue.setVisible(false);
		m_options.setVisible(false);
		m_exit.setVisible(false);
	}
	
	public void show(){
		m_new_game.setVisible(true);
		m_continue.setVisible(true);
		m_options.setVisible(true);
		m_exit.setVisible(true);
	}
	
	public void dispose(){
		m_mainmenuSkin.dispose();
	}
}
