package com.me.geneticcars.menu;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class TextItem extends Actor {

	private BitmapFont m_font;
	private String m_text;
	private float m_x;
	private float m_y;

	public TextItem(String text, float x, float y) {
		m_text = text;
		m_x = x;
		m_y = y;
	}

	public TextItem(BitmapFont font, String text, float x, float y) {
		m_font = font;
		m_text = text;
		m_x = x;
		m_y = y;
	}

	public String getText() {
		return m_text;
	}

	public void setFont(BitmapFont font) {
		m_font = font;
	}

	public void setText(String text) {
		m_text = text;
	}

	public void setPosition(float x, float y) {
		m_x = x;
		m_y = y;
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		m_font.draw(batch, m_text, m_x, m_y);
		super.draw(batch, parentAlpha);
	}
}
