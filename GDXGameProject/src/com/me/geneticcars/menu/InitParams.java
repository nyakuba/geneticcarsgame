package com.me.geneticcars.menu;

public class InitParams {
	public final int wheel_count;
	public final int mutation_rate;
	public final int car_count;
	public final int edges_count;
	public final int panel_count;
	public final int seed;
	
	public static final int PARAMS_COUNT = 6;
	
	public InitParams(int wc, int mr, int cc, int ec, int pc, int sd){
		wheel_count = wc;
		mutation_rate = mr;
		car_count = cc;
		edges_count = ec;
		panel_count = pc;
		seed = sd;
	}
	
	/**  
	 * @return int[] = {wc, mr, cc, ec, pc, sd};
	 */
	public int[] getParams(){
		int p[] = new int[PARAMS_COUNT];
		p[0] = wheel_count;
		p[1] = mutation_rate;
		p[2] = car_count;
		p[3] = edges_count;
		p[4] = panel_count;
		p[5] = seed;
		return p;
	}
}
