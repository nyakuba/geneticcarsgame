package com.me.geneticcars.menu;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.me.geneticcars.Car;
import com.me.geneticcars.CarParameters;
import com.me.geneticcars.PanelSet;
import com.me.geneticcars.menu.view.MainMenuScreen;

public class MenuWorld {
	static final float WORLD_TO_BOX = 0.01f;
	static final float BOX_TO_WORLD = 100f;
	static final float GRAVITY_X = 0f;
	static final float GRAVITY_Y = -3000f;

	private boolean m_touchIsActive;
	private Vector3 m_touchPos;
	private Fixture m_touchFixture;
	private Car m_cars[];
	private PanelSet m_ground;

	private OrthographicCamera m_camera;
	private World m_world;
	private Box2DDebugRenderer m_debugRenderer;
	private boolean isMoveInversed0;
	private boolean isMoveInversed1;

	public MenuWorld() {
		m_camera = new OrthographicCamera();
		m_camera.setToOrtho(false, MainMenuScreen.X_RESOLUTION,
				MainMenuScreen.Y_RESOLUTION);
		m_world = new World(new Vector2(GRAVITY_X, GRAVITY_Y), true);
		m_debugRenderer = new Box2DDebugRenderer();

		this.create();

		m_touchIsActive = false;
		m_touchPos = new Vector3(0, 0, 0);
		m_touchFixture = m_cars[0].getBodyFixture();
		isMoveInversed0 = false;
		isMoveInversed1 = false;
		
		
		
	}
	
	private void create(){
		createGround();

		try {

			createCars();

		} catch (Exception ex) {
			System.err.println(ex);
			dispose();
			// exit
			Gdx.app.exit();
			return;
		}
	}
	
	
	PolygonSpriteBatch psbatch = new PolygonSpriteBatch();
	PolygonRegion pregion;
	PolygonRegion pregion1;
	Array<Body> tmpBody = new Array<Body>();


	public void render() {


//		psbatch.setProjectionMatrix(m_camera.combined);
//
//		psbatch.begin();
//
//		Iterator<Body> it = m_world.getBodies();
//		while(it.hasNext()){			
//			Body b = it.next();
//			if(b.getUserData()!=null && b.getUserData() instanceof PolygonRegion){
//				PolygonRegion reg = (PolygonRegion) b.getUserData();
////				System.out.println(b.getPosition().x + " " + b.getPosition().y + " " + reg.getRegion().getRegionWidth() + " " + reg.getRegion().getRegionHeight() + " " + b.getAngle());
//				psbatch.draw(pregion, b.getPosition().x, b.getPosition().y,0,0,reg.getRegion().getRegionWidth(),reg.getRegion().getRegionHeight(),1,1,b.getAngle());
//			}
//		}
//		
//		psbatch.end();


		// process drag and drop of cars
		dragAndDrop();
		// logic of car's moving
		correctCarMove();

		m_camera.update();
		m_debugRenderer.render(m_world, m_camera.combined);
		m_world.step(1 / 300f, 6, 2);
	}

	public void dispose() {
		for (Car c : m_cars) {
			if (c != null)
				c.dispose();
		}
		m_ground.dispose();
		m_world.dispose();
	}

	private void createGround() {
		m_ground = new PanelSet(m_world);
		m_ground.add(new Vector2(0f, MainMenuScreen.Y_RESOLUTION), 100f, 10f,
				-90f);
		m_ground.add(100f, 10f, -70f);
		m_ground.add(100f, 10f, -50f);
		m_ground.add(100f, 10f, -30f);
		m_ground.add(100f, 10f, 40f);
		m_ground.add(60f, 10f, -10f);
		m_ground.add(60f, 10f, 10f);
		m_ground.add(100f, 10f, -40f);
		m_ground.add(100f, 10f, 30f);
		m_ground.add(100f, 10f, 50f);
		m_ground.add(100f, 10f, 70f);
		m_ground.add(100f, 10f, 90f);
	}

	private void createCars() throws Exception {
		Vector2 vert[] = { new Vector2(-20.0f, 20.0f),
				new Vector2(-50.0f, -10.0f), new Vector2(50.0f, -10.0f),
				new Vector2(20.0f, 20.0f) };
		int wheelCount = 3;
		Vector2 wheelPos[] = { new Vector2(-20.0f, 20.0f),
				new Vector2(-50.0f, -10.0f), new Vector2(50.0f, -10.0f) };
		float radius[] = { 20, 25, 30 };
		float speed[] = { 110, 120, 130 };

		CarParameters cparam = new CarParameters(vert, wheelCount, wheelPos,
				radius, speed);
		CarParameters cparam1 = (CarParameters) cparam.clone();

		cparam1.m_wheelSpeed[0] = -50;
		cparam1.m_wheelSpeed[1] = -60;
		cparam1.m_wheelSpeed[2] = -70;

		m_cars = new Car[2];

		m_cars[0] = new Car(cparam);
		m_cars[0]
				.create(m_world, new Vector2(100, MainMenuScreen.Y_RESOLUTION));

		m_cars[1] = new Car(cparam1);
		m_cars[1]
				.create(m_world, new Vector2(MainMenuScreen.X_RESOLUTION - 100,
						MainMenuScreen.Y_RESOLUTION));
		
		float points[] = {
				-20, 20,
				-50,-10,
				 50,-10,
				 20, 20
		};

//		Texture car_texture = new Texture(Gdx.files.internal("car_texture.png"));
//		TextureRegion car_texture_region = new TextureRegion(car_texture);
//		TextureRegion car_texture_region1 = new TextureRegion(car_texture);
//		pregion = new PolygonRegion(car_texture_region, points);

//		pregion1 = new PolygonRegion(car_texture_region1, points);
//		m_cars[0].getCarBody().setUserData(pregion);
	}

	// Drag and drop of m_cars[0] and m_cars[1]
	private void dragAndDrop() {
		// TODO: не забыть масштабировать, если данная функция будет нужна.
		// if (textInputActive)
		// return;
		if (Gdx.input.isTouched()) {
			m_touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			m_camera.unproject(m_touchPos);

			boolean contains0 = m_cars[0].getBodyFixture().testPoint(
					m_touchPos.x, m_touchPos.y);
			boolean contains1 = m_cars[1].getBodyFixture().testPoint(
					m_touchPos.x, m_touchPos.y);

			if (contains1) {
				m_touchIsActive = true;
				m_touchFixture = m_cars[1].getBodyFixture();
			}
			if (contains0) {
				m_touchIsActive = true;
				m_touchFixture = m_cars[0].getBodyFixture();
			}

			if (m_touchIsActive) {
				m_touchFixture.getBody().setTransform(m_touchPos.x,
						m_touchPos.y, 0);
			}

		} else {
			m_touchIsActive = false;
		}
	}

	// Determines car moving on the screen.
	// Инвертирует движение машины при приближении к границам экрана.
	private void correctCarMove() {
		if ((MainMenuScreen.X_RESOLUTION - m_cars[0].getPosition().x) < MainMenuScreen.X_RESOLUTION / 10) {
			if (!isMoveInversed0) {
				m_cars[0].inverseMoveDirection(m_world);
				isMoveInversed0 = true;
			}
		} else if ((m_cars[0].getPosition().x) < MainMenuScreen.X_RESOLUTION / 10) {
			if (!isMoveInversed0) {
				m_cars[0].inverseMoveDirection(m_world);
				isMoveInversed0 = true;
			}
		} else
			isMoveInversed0 = false;

		if ((MainMenuScreen.X_RESOLUTION - m_cars[1].getPosition().x) < MainMenuScreen.X_RESOLUTION / 10) {
			if (!isMoveInversed1) {
				m_cars[1].inverseMoveDirection(m_world);
				isMoveInversed1 = true;
			}
		} else if ((m_cars[1].getPosition().x) < MainMenuScreen.X_RESOLUTION / 10) {
			if (!isMoveInversed1) {
				m_cars[1].inverseMoveDirection(m_world);
				isMoveInversed1 = true;
			}
		} else
			isMoveInversed1 = false;
	}
}
