package com.me.geneticcars;

import java.io.IOException;
import java.util.List;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.geneticcars.menu.view.MainMenuScreen;

public class GeneticCars extends Game implements ScreenChange{
	
	private SpriteBatch batch;
	private BitmapFont font;

	private MainMenuScreen menu;
	private WorldScreen game;
	
	public void create() {
		batch = new SpriteBatch();
		font = new BitmapFont(Gdx.files.internal("my_font.fnt"), false);
		font.setColor(1f, 0f, 0f, 1f);
		// creates MainMenu Screen
		menu = new MainMenuScreen(this);
		this.setScreen(menu);
	}

	public void setMenuScreen(){
		this.setScreen(menu);
	}
	
	public void setGameScreen(){
		if(game == null)
			game = new WorldScreen(this, menu.getParams());
		this.setScreen(game);
	}
	
	public void render() {
		super.render(); // important!
	}

	public void dispose() {
		batch.dispose();
		font.dispose();
	}
	
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	// TODO: Архитектура - говно. Все переделать.
	@Override
	public void loadAndSetGameScreen() {
		IOUtils xml = new IOUtils("progress.xml");
		List<Chromosome> list = null;
		try {
			list = xml.getChromosomes();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(game != null){
			game.loadProgress(list);
			setGameScreen();
		} else {
			game = new WorldScreen(this, menu.getParams());
			game.loadProgress(list);
			this.setScreen(game);
		}
	}
}
