package com.me.geneticcars;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;

public class Car {

	// global parameters
	static final float BODY_DENSITY = 10f;
	static final float BODY_FRICTION = 15f;
	static final float BODY_RESTITUTION = 0.5f;
	static final float WHEEL_DENSITY = 5f;
	static final float WHEEL_FRICTION = 5.0f;
	static final float WHEEL_RESTITUTION = 0.5f;
	static final int MAX_WHEEL_COUNT = 3;
	static final int MAX_VERTICES_COUNT = 10;
	static final float VERTICES_RADIUS = 100.0f;
	static final float MIN_AREA = 3000.0f;

	// /first of all you must specify car Body and parameters of wheels.
	// /@param vertices[] - relative coordinates of vertices
	// /@param wheelCount - summary wheel count
	// /@param wheelPos[] - relative coordinates of wheels
	// /@param whhelRadius[] - wheel's radius
	// /@param wheelSpeed[] wheel's speed
	public Car(Vector2 vertices[], int wheelCount, Vector2 wheelPos[],
			float wheelRadius[], float wheelSpeed[]) throws Exception {
		boolean one = wheelCount <= wheelPos.length;
		boolean two = wheelCount <= wheelRadius.length;
		boolean three = wheelCount <= wheelSpeed.length;
		if (one && two && three) {
			m_wheelCount = wheelCount;
			m_vertices = vertices;
			m_wheelPos = wheelPos;
			m_wheelRadius = wheelRadius;
			m_wheelSpeed = wheelSpeed;
		} else {
			throw new Exception(
					"wheelCount > wheelPos[] || > wheelRadius[] || > wheelSpeed[]");
		}
	}

	public Car(CarParameters param) throws Exception {
		this(param.m_vertices, param.m_wheelCount, param.m_wheelPos,
				param.m_wheelRadius, param.m_wheelSpeed);
	}

	// /than you can create a car in the world
	public void create(World world) {
		m_world = world;
		Vector2 position = new Vector2(200, 300);
		// create body of a car
		m_bodyDef = new BodyDef();
		m_bodyDef.type = BodyType.DynamicBody;
		m_bodyDef.position.set(position);
		m_body = world.createBody(m_bodyDef);

		// collide only with default static bodies.
		Filter filter = new Filter();
		filter.maskBits = 0x0001;
		filter.categoryBits = 0x0002;

		// create fixture
		m_fixtureDef = new FixtureDef();
		// m_fixtureDef.shape = m_shape;
		m_fixtureDef.density = BODY_DENSITY;
		m_fixtureDef.friction = BODY_FRICTION;
		m_fixtureDef.restitution = BODY_RESTITUTION;
		// m_fixture = m_body.createFixture(m_fixtureDef);

		// ////////+++++++
		// ////////+++++++

		ArrayList<Vector2> startPoint = new ArrayList<Vector2>();

		for (int i = 0; i < m_vertices.length; i++) {
			startPoint.add(m_vertices[i].cpy());
		}

		ArrayList<Vector2> japanPoint;

		japanPoint = GeneticMath.japanSort(startPoint);
		
		// набор треугольников
		ArrayList<Vector2[]> Trig = new ArrayList<Vector2[]>();
		
		for (int i = 1; i < japanPoint.size() - 1; i++) {
			float x = japanPoint.get(i + 1).x - japanPoint.get(0).x;
			float y = japanPoint.get(i + 1).y - japanPoint.get(0).y;
			float x1 = japanPoint.get(i).x - japanPoint.get(0).x;
			float y1 = japanPoint.get(i).y - japanPoint.get(0).y;
			float diff_angle = new Vector2(x,y).angle() - new Vector2(x1,y1).angle();
			if (diff_angle < 180f) {
				Trig.add(new Vector2[3]);
				Trig.get(Trig.size() - 1)[0] = japanPoint.get(0);
				Trig.get(Trig.size() - 1)[1] = japanPoint.get(i);
				Trig.get(Trig.size() - 1)[2] = japanPoint.get(i + 1);
			}
		}

		// Нет, это не дублирование кода. Шаг между точками - два, в отличии от
		// случая с циклами.
		// Да, это костыль. Но, вроде, безобидный.
		// TODO: проверить все еще раз.
		{
			float x = japanPoint.get(1).x - japanPoint.get(0).x;
			float y = japanPoint.get(1).y - japanPoint.get(0).y;
			float x1 = japanPoint.get(japanPoint.size() - 1).x - japanPoint.get(0).x;
			float y1 = japanPoint.get(japanPoint.size() - 1).y - japanPoint.get(0).y;
			float diff_angle = new Vector2(x,y).angle() - new Vector2(x1,y1).angle();
			if (Math.abs(diff_angle) > 180f) {
				Trig.add(new Vector2[3]);
				Trig.get(Trig.size() - 1)[0] = japanPoint.get(0);
				Trig.get(Trig.size() - 1)[1] = japanPoint
						.get(japanPoint.size() - 1);
				Trig.get(Trig.size() - 1)[2] = japanPoint.get(1);
			}
		}

		m_formFixture = new Fixture[Trig.size()];
		m_formFixtureCount = Trig.size();
		m_area = 0;

		for (int i = 0; i < Trig.size(); i++) {
			PolygonShape shape = new PolygonShape();
			shape.set(Trig.get(i));
			m_fixtureDef.shape = shape;
			m_formFixture[i] = m_body.createFixture(m_fixtureDef);
			m_formFixture[i].setFilterData(filter);
			
			Vector2 vec1 = new Vector2(Trig.get(i)[1].x - Trig.get(i)[0].x, Trig.get(i)[1].y - Trig.get(i)[0].y);
			Vector2 vec2 =new Vector2(Trig.get(i)[2].x - Trig.get(i)[0].x, Trig.get(i)[2].y - Trig.get(i)[0].y);
			
//			System.out.println("vec1:" + vec1);
//			System.out.println("vec2:" + vec2);
//			System.out.println("sin:" + Math.sin(
//					Math.abs(Trig.get(i)[1].angle() - Trig.get(i)[2].angle())
//					/ 57.0f));
			//Прибавляем площадь текущего треугольника
			m_area += (1.0f / 2)
					*vec1.len()
					*vec2.len()
					*Math.sin(
							Math.abs(Trig.get(i)[1].angle() - Trig.get(i)[2].angle())
							/ 57.0f); 

		}

		//System.out.println(m_area);
		//делаем из тощих машинок паралитиков.
		if(m_area <= MIN_AREA){
			for(int i = 0; i < m_wheelCount; i++){
				m_wheelSpeed[i] = 0;	
			}
		}
		
		// create wheels
		m_wheelBodyDef = new BodyDef[m_wheelCount];
		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelBodyDef[i] = new BodyDef();
		}

		m_wheelBody = new Body[m_wheelCount];
		m_wheelShape = new CircleShape[m_wheelCount];

		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelShape[i] = new CircleShape();
		}

		m_wheelFixtureDef = new FixtureDef[m_wheelCount];
		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelFixtureDef[i] = new FixtureDef();
		}

		m_wheelFixture = new Fixture[m_wheelCount];

		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelBodyDef[i].type = BodyType.DynamicBody;
			Vector2 wheelPos = new Vector2(position);
			m_wheelBodyDef[i].position.set(wheelPos.add(m_wheelPos[i]));
			m_wheelBody[i] = world.createBody(m_wheelBodyDef[i]);
			m_wheelShape[i].setRadius(m_wheelRadius[i]);
			m_wheelFixtureDef[i].shape = m_wheelShape[i];
			m_wheelFixtureDef[i].density = WHEEL_DENSITY;
			m_wheelFixtureDef[i].friction = WHEEL_FRICTION;
			m_wheelFixtureDef[i].restitution = WHEEL_RESTITUTION;

			m_wheelFixture[i] = m_wheelBody[i]
					.createFixture(m_wheelFixtureDef[i]);
		}

		// create joints
		m_wheelJointDef = new RevoluteJointDef[m_wheelCount];
		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelJointDef[i] = new RevoluteJointDef();
		}
		m_wheelJoint = new Joint[m_wheelCount];
		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelJointDef[i].bodyA = m_body;
			m_wheelJointDef[i].bodyB = m_wheelBody[i];
			m_wheelJointDef[i].collideConnected = false;
			m_wheelJointDef[i].localAnchorA.set(m_wheelPos[i]);
			m_wheelJointDef[i].localAnchorB.set(0, 0);
			m_wheelJointDef[i].enableMotor = true;
			m_wheelJointDef[i].maxMotorTorque = Float.MAX_VALUE;
			m_wheelJointDef[i].motorSpeed = m_wheelSpeed[i] * (-1); // TODO:
																	// hardcode
			m_wheelJoint[i] = world.createJoint(m_wheelJointDef[i]);
		}

		// Collisions:
		// bool collide =
		// (filterA.maskBits & filterB.categoryBits) != 0 &&
		// (filterA.categoryBits & filterB.maskBits) != 0;
		// default values are 0x0001 for categoryBits and 0xFFFF for maskBits.
		// for more control use group indexes and ContactFilters

		// m_fixture.setFilterData(filter);
		for (Fixture f : m_wheelFixture) {
			f.setFilterData(filter);
		}

		/*
		 * for(int k = 0; k < m_wheelCount; k++){
		 * //m_body.destroyFixture(m_wheelFixture[k]);
		 * m_wheelBody[k].destroyFixture(m_wheelFixture[k]); }
		 */

	}

	public void create(World world, Vector2 position) {
		m_world = world;

		// create body of a car
		m_bodyDef = new BodyDef();
		m_bodyDef.type = BodyType.DynamicBody;
		m_bodyDef.position.set(position);
		m_body = world.createBody(m_bodyDef);
		// create shape
		m_shape = new PolygonShape();
		m_shape.set(m_vertices);
		// create fixture
		m_fixtureDef = new FixtureDef();
		m_fixtureDef.shape = m_shape;
		m_fixtureDef.density = BODY_DENSITY;
		m_fixtureDef.friction = BODY_FRICTION;
		m_fixtureDef.restitution = BODY_RESTITUTION;
		m_fixture = m_body.createFixture(m_fixtureDef);

		// create wheels
		m_wheelBodyDef = new BodyDef[m_wheelCount];
		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelBodyDef[i] = new BodyDef();
		}
		m_wheelBody = new Body[m_wheelCount];
		m_wheelShape = new CircleShape[m_wheelCount];
		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelShape[i] = new CircleShape();
		}
		m_wheelFixtureDef = new FixtureDef[m_wheelCount];
		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelFixtureDef[i] = new FixtureDef();
		}
		m_wheelFixture = new Fixture[m_wheelCount];
		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelBodyDef[i].type = BodyType.DynamicBody;
			Vector2 wheelPos = new Vector2(position);
			m_wheelBodyDef[i].position.set(wheelPos.add(m_wheelPos[i]));
			m_wheelBody[i] = world.createBody(m_wheelBodyDef[i]);
			m_wheelShape[i].setRadius(m_wheelRadius[i]);
			m_wheelFixtureDef[i].shape = m_wheelShape[i];
			m_wheelFixtureDef[i].density = WHEEL_DENSITY;
			m_wheelFixtureDef[i].friction = WHEEL_FRICTION;
			m_wheelFixtureDef[i].restitution = WHEEL_RESTITUTION;
			m_wheelFixture[i] = m_wheelBody[i]
					.createFixture(m_wheelFixtureDef[i]);
		}

		// create joints
		m_wheelJointDef = new RevoluteJointDef[m_wheelCount];
		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelJointDef[i] = new RevoluteJointDef();
		}
		m_wheelJoint = new Joint[m_wheelCount];
		for (int i = 0; i < m_wheelCount; i++) {
			m_wheelJointDef[i].bodyA = m_body;
			m_wheelJointDef[i].bodyB = m_wheelBody[i];
			m_wheelJointDef[i].collideConnected = false;
			m_wheelJointDef[i].localAnchorA.set(m_wheelPos[i]);
			m_wheelJointDef[i].localAnchorB.set(0, 0);
			m_wheelJointDef[i].enableMotor = true;
			m_wheelJointDef[i].maxMotorTorque = Float.MAX_VALUE;
			m_wheelJointDef[i].motorSpeed = m_wheelSpeed[i];
			m_wheelJoint[i] = world.createJoint(m_wheelJointDef[i]);
		}

		// Collisions:
		// bool collide =
		// (filterA.maskBits & filterB.categoryBits) != 0 &&
		// (filterA.categoryBits & filterB.maskBits) != 0;
		// default values are 0x0001 for categoryBits and 0xFFFF for maskBits.
		// for more control use group indexes and ContactFilters

		// collide only with default static bodies.
		Filter filter = new Filter();
		filter.maskBits = 0x0001;
		filter.categoryBits = 0x0002;

		m_fixture.setFilterData(filter);
		for (Fixture f : m_wheelFixture) {
			f.setFilterData(filter);
		}
	}

	// //////////////////вывести машину из игры
	public void mustDie() {
		m_body.setActive(false);
		for (int i = 0; i < m_wheelCount; i++)
			m_wheelBody[i].setActive(false);
		m_imDie = true;
	}

	public void dispose() {

		/*
		 * for(int k = 0; k < m_formFixtureCount; k++){
		 * m_body.destroyFixture(m_formFixture[k]); }
		 * 
		 * for(int k = 0; k < m_wheelCount; k++){
		 * m_wheelBody[k].destroyFixture(m_wheelFixture[k]); }
		 */
		for (int k = 0; k < m_wheelCount; k++) {
			m_world.destroyJoint(m_wheelJoint[k]);
		}

		if (m_world != null)
			m_world.destroyBody(m_body);

		for (int k = 0; k < m_wheelCount; k++) {
			m_world.destroyBody(m_wheelBody[k]);
		}

		// m_shape.dispose();
		/*
		 * for(CircleShape wheelShape : m_wheelShape){ wheelShape.dispose(); }
		 */
	}

	// get methods
	public final Body getCarBody() {
		return m_body;
	}

	public final Vector2 getPosition() {
		return m_body.getPosition();
	}

	public final Vector2 getLinearVelocity() {
		return m_body.getLinearVelocity();
	}

	public final Fixture getBodyFixture() {
		return m_fixture;
	}

	public void inverseMoveDirection(World world) {
		for (int i = 0; i < m_wheelCount; i++) {
			world.destroyJoint(m_wheelJoint[i]);
			m_wheelJointDef[i].motorSpeed = -m_wheelJointDef[i].motorSpeed;
			m_wheelJoint[i] = world.createJoint(m_wheelJointDef[i]);
		}
	}

	private World m_world;

	// body
	private Body m_body;
	private BodyDef m_bodyDef;
	private PolygonShape m_shape;
	private Vector2 m_vertices[];
	private Fixture m_fixture;
	private Fixture m_formFixture[];
	private int m_formFixtureCount;
	private FixtureDef m_fixtureDef;

	// wheels
	private Body m_wheelBody[];
	private BodyDef m_wheelBodyDef[];
	private CircleShape m_wheelShape[];
	private Fixture m_wheelFixture[];
	private FixtureDef m_wheelFixtureDef[];
	// wheel parameters
	private int m_wheelCount;
	private float m_wheelSpeed[];
	private float m_wheelRadius[];
	// joints
	private Joint m_wheelJoint[];
	private RevoluteJointDef m_wheelJointDef[];
	private Vector2 m_wheelPos[];

	private float m_area = 0;
	boolean m_imDie = false;
}
