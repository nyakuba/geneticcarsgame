package com.me.geneticcars;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.badlogic.gdx.math.Vector2;

public class IOUtils {
	private final String m_filename;
	private XMLEventWriter event_writer;
	FileOutputStream file_output_stream;
	private boolean isOpened;
	
	public IOUtils(String filename){ 
		m_filename = filename;
		isOpened = false;
	}
	
	public boolean isOpened(){
		return isOpened;
	}
	
	public void create(){
		try {
			file_output_stream = new FileOutputStream(m_filename);
			XMLOutputFactory output_factory = XMLOutputFactory.newInstance();
			event_writer = output_factory.createXMLEventWriter(file_output_stream);
			//create factories
			XMLEventFactory event_factory = XMLEventFactory.newInstance();
			// create main writer
			
			// document
			StartDocument start_document = event_factory.createStartDocument("UTF-8");
			event_writer.add(start_document);
			event_writer.add(event_factory.createDTD("\n"));
			// main tag
			StartElement population = event_factory.createStartElement("", "", "population");
			event_writer.add(population);
			event_writer.add(event_factory.createDTD("\n"));
			
		} catch(XMLStreamException | IOException e) {
			e.printStackTrace();
		}
		isOpened = true;
	}
	
	public void addToXML(Vector2 position){
		try {
			
			XMLEventFactory event_factory = XMLEventFactory.newInstance();
			// helpers
			XMLEvent end = event_factory.createDTD("\n");
			XMLEvent tab = event_factory.createDTD("\t");
			event_writer.add(event_factory.createStartElement("", "", "position"));
			event_writer.add(end);
			createNode(event_writer, "x", ""+position.x);
			createNode(event_writer, "y", ""+position.y);
			event_writer.add(event_factory.createEndElement("", "", "position"));
			event_writer.add(end);
			
		} catch (XMLStreamException e){
			e.printStackTrace();
		} 
	}
	
	public void addToXML(Chromosome chr){
		try {
			
			XMLEventFactory event_factory = XMLEventFactory.newInstance();
			// helpers
			XMLEvent end = event_factory.createDTD("\n");
			XMLEvent tab = event_factory.createDTD("\t");
			// chromosome tag
			event_writer.add(event_factory.createStartElement("", "", "chromosome"));
			event_writer.add(end);
			createNode(event_writer, "vertices_count", ""+chr.m_vertices_count);
			for(int i=0; i<chr.m_vertices_count; i++){
				event_writer.add(tab);
				event_writer.add(event_factory.createStartElement("", "", "vertice"));
				event_writer.add(end);
				event_writer.add(tab);
				createNode(event_writer, "x", ""+chr.m_vertices.get(i).x);
				event_writer.add(tab);
				createNode(event_writer, "y", ""+chr.m_vertices.get(i).y);
				event_writer.add(tab);
				event_writer.add(event_factory.createEndElement("", "", "vertice"));
				event_writer.add(end);
			}
			createNode(event_writer, "wheel_count", ""+chr.m_wheel_count);
			for(int i=0; i<chr.m_wheel_count; i++){
				createNode(event_writer, "wheel_size", ""+chr.m_wheel_size.get(i));
			}
			for(int i=0; i<chr.m_wheel_count; i++){
				createNode(event_writer, "wheel_speed", ""+chr.m_wheel_speed.get(i));
			}
			for(int i=0; i<chr.m_wheel_count; i++){
				createNode(event_writer, "wheel_vertices_index", ""+chr.m_wheel_vertices_index.get(i));
			}
			
			event_writer.add(event_factory.createEndElement("", "", "chromosome"));
			event_writer.add(end);
			// end of chromosome
			
		} catch (XMLStreamException e){
			e.printStackTrace();
		} 
	}
	
	public List<Chromosome> getChromosomes() throws IOException{
		List<Chromosome> list = new LinkedList<Chromosome>();
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		DocumentBuilder db = null;
		Document dom = null;
		try {
			
			db = dbf.newDocumentBuilder();
			dom = db.parse(m_filename);
			
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		} 
		
		//get the root element
		Element docEle = dom.getDocumentElement();

		//get a nodelist of elements
		NodeList nl = docEle.getElementsByTagName(CHROMOSOME);
		if(nl != null && nl.getLength() > 0) {
			for(int i = 0 ; i < nl.getLength();i++) {
				Element elem = (Element)nl.item(i);
				Chromosome chr = getChromosome(elem);
				list.add(chr);
			}
		}
		return list;
	}
	
	public List<Vector2> getPositions() throws IOException{
		List<Vector2> list = new LinkedList<Vector2>();
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		DocumentBuilder db = null;
		Document dom = null;
		try {
			
			db = dbf.newDocumentBuilder();
			dom = db.parse(m_filename);
			
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		} 
		
		//get the root element
		Element docEle = dom.getDocumentElement();

		//get a nodelist of elements
		NodeList nl = docEle.getElementsByTagName(POSITION);
		if(nl != null && nl.getLength() > 0) {
			for(int i = 0 ; i < nl.getLength();i++) {
				Element elem = (Element)nl.item(i);
				float x = Float.valueOf(elem.getElementsByTagName("x").item(0).getFirstChild().getNodeValue());
				float y = Float.valueOf(elem.getElementsByTagName("y").item(0).getFirstChild().getNodeValue());
				list.add(new Vector2(x,y));
			}
		}
		return list;
	}
	
	public void close(){
		if(isOpened){
			try {
				
				event_writer.add(XMLEventFactory.newInstance().createEndElement("", "", "population"));
				event_writer.add(XMLEventFactory.newInstance().createEndDocument());
				event_writer.close();
				file_output_stream.close();
				
			} catch (XMLStreamException | FactoryConfigurationError | IOException e) {
				e.printStackTrace();
			}
		}
	}

	private final String POSITION = "position";
	private final String CHROMOSOME = "chromosome";
	private final String VCOUNT = "vertices_count";
	private final String VERTICES = "vertice";
	private final String WCOUNT = "wheel_count";
	private final String WSIZE = "wheel_size";
	private final String WSPEED = "wheel_speed";
	private final String WVINDEX = "wheel_vertices_index";
	
	private Chromosome getChromosome(Element elem){
		int vertices_count = 0;
		ArrayList<Vector2> vertices = new ArrayList<Vector2>(Chromosome.MAX_VERTICES_COUNT);
		int wheel_count = 0;
		ArrayList<Integer> wheel_size = new ArrayList<Integer>(Chromosome.MAX_WHEEL_COUNT);
		ArrayList<Integer> wheel_speed = new ArrayList<Integer>(Chromosome.MAX_WHEEL_COUNT);
		ArrayList<Integer> wheel_vertices_index = new ArrayList<Integer>(Chromosome.MAX_WHEEL_COUNT);
		{
			List<String> l = getTextValues(elem, VCOUNT);
			if(l.size() > 0)
				vertices_count = Integer.valueOf(l.get(0));
		}
		{
			List<String> l = getTextValues(elem, WCOUNT);
			if(l.size() > 0)
				wheel_count = Integer.valueOf(l.get(0));
		}
		{
			NodeList nl = elem.getElementsByTagName(VERTICES);
			if(nl != null) {
				for(int i=0; i<nl.getLength(); i++){
					Element el = (Element)nl.item(i);
					float x = Float.valueOf(el.getElementsByTagName("x").item(0).getFirstChild().getNodeValue());
					float y = Float.valueOf(el.getElementsByTagName("y").item(0).getFirstChild().getNodeValue());
					vertices.add(new Vector2(x,y));
				}
			}
		}
		{
			List<String> l = getTextValues(elem, VCOUNT);
			if(l.size() > 0)
				vertices_count = Integer.valueOf(l.get(0));
		}
		{
			List<String> l = getTextValues(elem, WSIZE);
			for(String str : l){
				wheel_size.add(Integer.valueOf(str));
			}
		}
		{
			List<String> l = getTextValues(elem, WSPEED);
			for(String str : l){
				wheel_speed.add(Integer.valueOf(str));
			}
		}
		{
			List<String> l = getTextValues(elem, WVINDEX);
			for(String str : l){
				wheel_vertices_index.add(Integer.valueOf(str));
			}
		}
		return new Chromosome(vertices_count, vertices, wheel_count, wheel_size, wheel_speed, wheel_vertices_index);
	}
	
	private List<String> getTextValues(Element elem, String tag_name){
		LinkedList<String> values = new LinkedList<String>();
		NodeList nl = elem.getElementsByTagName(tag_name);
		if(nl != null && nl.getLength() > 0){
			for(int i=0; i<nl.getLength(); i++){
				Element e = (Element) nl.item(i);
				values.add(e.getFirstChild().getNodeValue());
			}
		}
		return values;
	}
	
	
	private void createNode(XMLEventWriter event_writer, String name, String value) throws XMLStreamException{
		
	    XMLEventFactory eventFactory = XMLEventFactory.newInstance();
	    XMLEvent end = eventFactory.createDTD("\n");
	    XMLEvent tab = eventFactory.createDTD("\t");
	    // create Start node
	    StartElement sElement = eventFactory.createStartElement("", "", name);
	    event_writer.add(tab);
	    event_writer.add(sElement);
	    // create Content
	    Characters characters = eventFactory.createCharacters(value);
	    event_writer.add(characters);
	    // create End node
	    EndElement eElement = eventFactory.createEndElement("", "", name);
	    event_writer.add(eElement);
	    event_writer.add(end);
	    
	}
}
