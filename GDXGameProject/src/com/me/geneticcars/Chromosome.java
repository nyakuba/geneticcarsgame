package com.me.geneticcars;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.google.common.collect.ImmutableList;

public class Chromosome {

	public static final int MAX_WHEEL_COUNT = 5;
	public static final int MIN_VERTICES_COUNT = 3;
	public static final int MAX_VERTICES_COUNT = 10;
	public static final int MIN_WHEEL_SIZE = 10;
	public static final int MAX_WHEEL_SIZE = 60;
	public static final int MAX_WHEEL_SPEED = 80;
	public static final int MAX_VERTICE_SIZE = 200;
	public static final int MAX_MUTATION_RATE = 100;

	/** (x+y)*MAX_V_C + (w_si+w_sp+w_v_i)*MAX_W_C */
	public static final int params_count = 2 * MAX_VERTICES_COUNT + 3
			* MAX_WHEEL_COUNT; // 35

	public final int m_vertices_count;
	public ImmutableList<Coord> m_vertices;

	public final int m_wheel_count;
	public ImmutableList<Integer> m_wheel_size;
	public ImmutableList<Integer> m_wheel_speed;
	public ImmutableList<Integer> m_wheel_vertices_index;

	public Chromosome(Chromosome param) {
		m_vertices_count = param.m_vertices_count;
		m_vertices = param.m_vertices;
		m_wheel_count = param.m_wheel_count;
		m_wheel_size = param.m_wheel_size;
		m_wheel_speed = param.m_wheel_speed;
		m_wheel_vertices_index = param.m_wheel_vertices_index;
	}

	public Chromosome(int vertices_count, Vector2[] vertices, int wheel_count,
			int[] wheel_size, int[] wheel_speed, int[] wheel_vertices_index) {
		try {
			isInputParamsValid(vertices_count, vertices, wheel_count,
					wheel_size, wheel_speed, wheel_vertices_index);
		} catch (IllegalArgumentException exc) {
			System.err.println(exc.getMessage());
			exc.printStackTrace();
			Gdx.app.exit();
		}

		ImmutableList.Builder<Coord> vb = new ImmutableList.Builder<Coord>();
		for (int i = 0; i < vertices_count; i++)
			vb.add(new Coord(vertices[i]));
		ImmutableList.Builder<Integer> wsib = new ImmutableList.Builder<Integer>();
		ImmutableList.Builder<Integer> wspb = new ImmutableList.Builder<Integer>();
		ImmutableList.Builder<Integer> wvib = new ImmutableList.Builder<Integer>();
		for (int i = 0; i < wheel_count; i++) {
			wsib.add(new Integer(wheel_size[i]));
			wspb.add(new Integer(wheel_speed[i]));
			wvib.add(new Integer(wheel_vertices_index[i]));
		}

		m_vertices_count = vertices_count;
		m_vertices = vb.build();
		m_wheel_count = wheel_count;
		m_wheel_size = wsib.build();
		m_wheel_speed = wspb.build();
		m_wheel_vertices_index = wvib.build();
	}

	public Chromosome(int vertices_count, List<Vector2> vertices,
			int wheel_count, List<Integer> wheel_size,
			List<Integer> wheel_speed, List<Integer> wheel_vertices_index) {
		try {
			isInputParamsValid(vertices_count, vertices, wheel_count,
					wheel_size, wheel_speed, wheel_vertices_index);
		} catch (IllegalArgumentException exc) {
			System.err.println(exc.getMessage());
			exc.printStackTrace();
			Gdx.app.exit();
		}

		ImmutableList.Builder<Coord> vb = new ImmutableList.Builder<Coord>();
		for (int i = 0; i < vertices_count; i++)
			vb.add(new Coord(vertices.get(i)));
		
		m_vertices_count = vertices_count;
		m_vertices = vb.build();
		m_wheel_count = wheel_count;
		m_wheel_size = ImmutableList.copyOf(wheel_size);
		m_wheel_speed = ImmutableList.copyOf(wheel_speed);
		m_wheel_vertices_index = ImmutableList.copyOf(wheel_vertices_index);
	}

	/**
	 * @return Vector2 array. Copy of m_vertices.
	 */
	public Vector2[] getVector2Array() {
		Vector2 ret[] = new Vector2[m_vertices_count];
		for (int i = 0; i < m_vertices_count; i++)
			ret[i] = m_vertices.get(i).toVector2();
		return ret;
	}

	// for easier debug
	@Override
	public String toString() {
		return "vertices_count:" + m_vertices_count + "\n" + "vertices: "
				+ m_vertices + "\n" + "wheel_count:" + m_wheel_count + "\n"
				+ "wheel_size: " + m_wheel_size + "\n" + "wheel_speed: "
				+ m_wheel_speed + "\n" + "wheel_vertices_index: "
				+ m_wheel_vertices_index;
	}

	/**
	 * Crossbreeding between two cars.
	 * 
	 * @param partner
	 *            another car params
	 * @param gen
	 *            random numbers generator
	 * @return
	 */
	public Chromosome toBeABadGirl(Chromosome partner, Random gen) {
		// hard porn
		// генератор можно задать самим или по умолчанию.
		// скрещивание происходит отдельно по массивам граней и колес.
		// гарантируется, что в результате скрещивания в массиве потомка
		// активные грани и колеса будут находиться в начале массива
		// при условии, что они были в начале у отца и матери.
		// в массиве случайно выбирается точка скрещивания.
		// она делит массив на две части: первая часть заполняется генами отца,
		// вторая - матери.
		if (gen == null)
			gen = new Random();

		Chromosome father = this;
		Chromosome mother = partner;
		Chromosome child;

		int vert_cross_point = gen.nextInt(MAX_VERTICES_COUNT);
		int wh_cross_point = gen.nextInt(MAX_WHEEL_COUNT);

		int vertices_count = 0;
		ArrayList<Vector2> vertices = new ArrayList<Vector2>(MAX_VERTICES_COUNT);
		{
			for (int i=0; i < father.m_vertices_count && i < vert_cross_point; i++)
				vertices.add(father.m_vertices.get(i).toVector2());
			for(int j=vert_cross_point; j < mother.m_vertices_count; j++){
				vertices.add(mother.m_vertices.get(j).toVector2());
			}
			vertices_count = vertices.size();
		}

		int wheel_count = 0;
		ArrayList<Integer> wheel_size = new ArrayList<Integer>(MAX_WHEEL_COUNT);
		ArrayList<Integer> wheel_speed = new ArrayList<Integer>(MAX_WHEEL_COUNT);
		ArrayList<Integer> wheel_vertices_index = new ArrayList<Integer>(
				MAX_WHEEL_COUNT);
		{
			for (int i=0; i < father.m_wheel_count && i < wh_cross_point; i++) {
				wheel_size.add(father.m_wheel_size.get(i));
				wheel_speed.add(father.m_wheel_speed.get(i));
				wheel_vertices_index.add(father.m_wheel_vertices_index.get(i)%vertices_count);
			}
			for(int j = wh_cross_point; j < mother.m_wheel_count; j++) {
				wheel_size.add(mother.m_wheel_size.get(j));
				wheel_speed.add(mother.m_wheel_speed.get(j));
				int new_vert_index = mother.m_wheel_vertices_index.get(j)%vertices_count;
				if(!wheel_vertices_index.contains(new_vert_index))
					wheel_vertices_index.add(mother.m_wheel_vertices_index.get(j));
			}
			wheel_count = wheel_vertices_index.size();
		}

		child = new Chromosome(vertices_count, vertices, wheel_count,
				wheel_size, wheel_speed, wheel_vertices_index);
		return child;
	}

	/**
	 * Mutation. Change single random parameter. Wheel_count increases only if
	 * wheel_vertices_index increases.
	 * 
	 * @param mutation_rate
	 *            mutation chance
	 * @param gen
	 *            random number generator
	 * @return mutated this CarParam object
	 */
	public Chromosome mutate(int mutation_rate, Random gen) {
		if (gen == null)
			gen = new Random();
		boolean mutable = Math.abs(gen.nextInt() % MAX_MUTATION_RATE) < mutation_rate;
		// если попали в данную вероятность мутации
		if (mutable) {
			int vertices_count = m_vertices_count;
			ArrayList<Vector2> vertices = new ArrayList<Vector2>(MAX_VERTICES_COUNT);
			for(Coord c : m_vertices)
				vertices.add(c.toVector2());
			int wheel_count = m_wheel_count;
			ArrayList<Integer> wheel_size = new ArrayList<Integer>(MAX_WHEEL_COUNT);
			ArrayList<Integer> wheel_speed = new ArrayList<Integer>(MAX_WHEEL_COUNT);
			ArrayList<Integer> wheel_vertices_index = new ArrayList<Integer>(MAX_WHEEL_COUNT);
			wheel_size.addAll(m_wheel_size);
			wheel_speed.addAll(m_wheel_speed);
			wheel_vertices_index.addAll(m_wheel_vertices_index);
			
			int mut_param_index = Math.abs(gen.nextInt(params_count));
			if(mut_param_index < MAX_VERTICES_COUNT*2){
				if(mut_param_index/2 < m_vertices_count){
					int new_value = gen.nextInt(MAX_VERTICE_SIZE);
					if(mut_param_index%2 == 0){
						vertices.get(mut_param_index/2).x = new_value;
					} else
						vertices.get(mut_param_index/2).y = new_value;
				} else {
					int new_x = gen.nextInt(MAX_VERTICE_SIZE);
					int new_y = gen.nextInt(MAX_VERTICE_SIZE);
					vertices.add(new Vector2(new_x, new_y));
					vertices_count++;
				}
			} else if(mut_param_index < MAX_VERTICES_COUNT*2 + MAX_WHEEL_COUNT){
				mut_param_index -= MAX_VERTICES_COUNT*2;
				int new_size = Math.abs(gen.nextInt(MAX_WHEEL_SIZE));
				if(new_size < MIN_WHEEL_SIZE)
					new_size = MIN_WHEEL_SIZE;
				if(wheel_size.size() <= mut_param_index){
					boolean added = addNewWheel(wheel_vertices_index, wheel_size, wheel_speed, gen);
					if(added)
						wheel_count++;
				} else
					wheel_size.set(mut_param_index, new_size);
			} else if(mut_param_index < MAX_VERTICES_COUNT*2 + MAX_WHEEL_COUNT*2){
				mut_param_index -= MAX_VERTICES_COUNT*2 + MAX_WHEEL_COUNT;
				int new_speed = Math.abs(gen.nextInt(MAX_WHEEL_SPEED));
				if(wheel_speed.size() <= mut_param_index){
					boolean added = addNewWheel(wheel_vertices_index, wheel_size, wheel_speed, gen);
					if(added)
						wheel_count++;
				} else
					wheel_speed.set(mut_param_index, new_speed);
			} else {
				mut_param_index -= MAX_VERTICES_COUNT*2 + MAX_WHEEL_COUNT*2;
				int new_index = Math.abs(gen.nextInt(m_vertices_count));
				if(mut_param_index < m_wheel_count){
					if(wheel_vertices_index.get(mut_param_index) != new_index){
						if(wheel_vertices_index.contains(new_index)){
							wheel_vertices_index.remove(mut_param_index);
							wheel_size.remove(mut_param_index);
							wheel_speed.remove(mut_param_index);
							wheel_count--;
						} else {
							wheel_vertices_index.set(mut_param_index, new_index);
						}
					}
				} else {
					boolean added = addNewWheel(wheel_vertices_index, wheel_size, wheel_speed, gen);
					if(added)
						wheel_count++;
				}
			}
			
			return new Chromosome(vertices_count, vertices, wheel_count, wheel_size, wheel_speed, wheel_vertices_index);
		}
		return this;
	}
	
	private boolean addNewWheel(List<Integer> wheel_vertices_index, List<Integer> wheel_size, List<Integer> wheel_speed, Random gen){
		int new_index = gen.nextInt(m_vertices_count);
		if(!wheel_vertices_index.subList(0, m_wheel_count).contains(new_index)){
			wheel_vertices_index.add(new_index);
			if(wheel_size.size()<=MAX_WHEEL_COUNT){
				int new_size = gen.nextInt(MAX_WHEEL_SIZE);
				if(new_size < MIN_WHEEL_SIZE)
					new_size = MIN_WHEEL_SIZE;
				wheel_size.add(new_size);
			}
			if(wheel_speed.size()<=MAX_WHEEL_COUNT){
				int new_speed = gen.nextInt(MAX_WHEEL_SPEED);
				wheel_speed.add(new_speed);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Check if input parameters is valid.
	 * 
	 * @param vertices_count
	 *            should be < MAX_VERTICES_COUNT and > MIN_VERTICES_COUNT
	 * @param vertices
	 *            array length should be >= vertices_count and element's
	 *            components x,y should be < MAX_VERTICE_SIZE
	 * @param wheel_count
	 *            should be < MAX_WHEEL_COUNT and > 0
	 * @param wheel_size
	 *            array length should be >= wheel_count and elements should be >
	 *            MIN_WHEEL_SIZE and < MAX_WHEEL_SIZE
	 * @param wheel_speed
	 *            array length should be >= wheel_count and elements should be >
	 *            0 and < MAX_WHEEL_SPEED
	 * @param wheel_vertices_index
	 *            array length should be >= wheel_count and element should be >
	 *            0 and < vertices_count
	 * @return true or throws exception
	 * @throws IllegalArgumentException
	 */
	private boolean isInputParamsValid(int vertices_count, Vector2[] vertices,
			int wheel_count, int[] wheel_size, int[] wheel_speed,
			int[] wheel_vertices_index) throws IllegalArgumentException {
		// null pointer
		if (vertices == null)
			throw new IllegalArgumentException("vertices is null");

		if (wheel_size == null)
			throw new IllegalArgumentException("wheel_size is null");

		if (wheel_speed == null)
			throw new IllegalArgumentException("wheel_speed is null");

		if (wheel_vertices_index == null)
			throw new IllegalArgumentException("wheel_vertices_index is null");

		// count
		if (!(vertices_count <= MAX_VERTICES_COUNT && vertices_count >= MIN_VERTICES_COUNT))
			throw new IllegalArgumentException("invalid vertices count:"
					+ vertices_count);

		if (!(wheel_count <= vertices_count && wheel_count <= MAX_WHEEL_COUNT && wheel_count >= 0))
			throw new IllegalArgumentException("invalid wheel_count:"
					+ wheel_count);

		// array length
		if (!(vertices.length >= vertices_count))
			throw new IllegalArgumentException("invalid vertices length:"
					+ vertices.length);

		if (!(wheel_size.length >= wheel_count))
			throw new IllegalArgumentException("invalid wheel_size length:"
					+ wheel_size.length + "<" + wheel_count);

		if (!(wheel_speed.length >= wheel_count))
			throw new IllegalArgumentException("invalid wheel_sreed length:"
					+ wheel_speed.length + "<" + wheel_count);

		if (!(wheel_vertices_index.length >= wheel_count))
			throw new IllegalArgumentException(
					"invalid wheel_vertices_index length:"
							+ wheel_vertices_index.length + "<" + wheel_count);

		// array elements
		for (int i = 0; i < vertices_count; i++)
			if (vertices[i] == null) {
				throw new IllegalArgumentException(
						"vertices is not initialized: null pointer");
			} else if (Math.abs(vertices[i].x) > MAX_VERTICE_SIZE
					|| Math.abs(vertices[i].y) > MAX_VERTICE_SIZE) {
				throw new IllegalArgumentException("invalid vertice value: x="
						+ vertices[i].x + " y=" + vertices[i].y);
			}

		for (int i = 0; i < wheel_count; i++) {
			if (!(wheel_size[i] >= MIN_WHEEL_SIZE && wheel_size[i] <= MAX_WHEEL_SIZE))
				throw new IllegalArgumentException("invalid wheel_size[" + i
						+ "]:" + wheel_size[i]);

			if (!(wheel_speed[i] >= 0 && wheel_speed[i] <= MAX_WHEEL_SPEED))
				throw new IllegalArgumentException("invalid wheel_speed[" + i
						+ "]:" + wheel_speed[i]);

			if (!(wheel_vertices_index[i] >= 0 && wheel_vertices_index[i] < vertices_count))
				throw new IllegalArgumentException(
						"invalid wheel_vertices_index[" + i + "]:"
								+ wheel_vertices_index[i]);
		}
		return true;
	}
	
	private boolean isInputParamsValid(int vertices_count, List<Vector2> vertices,
			int wheel_count, List<Integer> wheel_size, List<Integer> wheel_speed,
			List<Integer> wheel_vertices_index) throws IllegalArgumentException {
		// null pointer
		if (vertices == null)
			throw new IllegalArgumentException("vertices is null");

		if (wheel_size == null)
			throw new IllegalArgumentException("wheel_size is null");

		if (wheel_speed == null)
			throw new IllegalArgumentException("wheel_speed is null");

		if (wheel_vertices_index == null)
			throw new IllegalArgumentException("wheel_vertices_index is null");

		// count
		if (!(vertices_count <= MAX_VERTICES_COUNT && vertices_count >= MIN_VERTICES_COUNT))
			throw new IllegalArgumentException("invalid vertices count:"
					+ vertices_count);

		if (!(wheel_count <= vertices_count && wheel_count <= MAX_WHEEL_COUNT && wheel_count >= 0))
			throw new IllegalArgumentException("invalid wheel_count:"
					+ wheel_count);

		// array length
		if (!(vertices.size() >= vertices_count))
			throw new IllegalArgumentException("invalid vertices length:"
					+ vertices.size());

		if (!(wheel_size.size() >= wheel_count))
			throw new IllegalArgumentException("invalid wheel_size length:"
					+ wheel_size.size() + "<" + wheel_count);

		if (!(wheel_speed.size() >= wheel_count))
			throw new IllegalArgumentException("invalid wheel_sreed length:"
					+ wheel_speed.size() + "<" + wheel_count);

		if (!(wheel_vertices_index.size() >= wheel_count))
			throw new IllegalArgumentException(
					"invalid wheel_vertices_index length:"
							+ wheel_vertices_index.size() + "<" + wheel_count);

		// array elements
		for (int i = 0; i < vertices_count; i++)
			if (vertices.get(i) == null) {
				throw new IllegalArgumentException(
						"vertices is not initialized: null pointer");
			} else if (Math.abs(vertices.get(i).x) > MAX_VERTICE_SIZE
					|| Math.abs(vertices.get(i).y) > MAX_VERTICE_SIZE) {
				throw new IllegalArgumentException("invalid vertice value: x="
						+ vertices.get(i).x + " y=" + vertices.get(i).y);
			}

		for (int i = 0; i < wheel_count; i++) {
			if (!(wheel_size.get(i) >= MIN_WHEEL_SIZE && wheel_size.get(i) <= MAX_WHEEL_SIZE))
				throw new IllegalArgumentException("invalid wheel_size[" + i
						+ "]:" + wheel_size.get(i));

			if (!(wheel_speed.get(i) >= 0 && wheel_speed.get(i) <= MAX_WHEEL_SPEED))
				throw new IllegalArgumentException("invalid wheel_speed[" + i
						+ "]:" + wheel_speed.get(i));

			if (!(wheel_vertices_index.get(i) >= 0 && wheel_vertices_index.get(i) < vertices_count))
				throw new IllegalArgumentException(
						"invalid wheel_vertices_index[" + i + "]:"
								+ wheel_vertices_index.get(i));
		}
		return true;
	}

	public class Coord {
		public final float x;
		public final float y;

		public Coord() {
			x = 0;
			y = 0;
		}

		public Coord(float ix, float iy) {
			x = ix;
			y = iy;
		}

		public Coord(final Vector2 v) {
			x = v.x;
			y = v.y;
		}

		public Vector2 toVector2() {
			return new Vector2(x, y);
		}

		@Override
		public String toString() {
			return "(" + x + " ," + y + ")";
		}

		@Override
		public boolean equals(Object obj) {
			Coord c = (Coord) obj;
			return c.x == x && c.y == y;
		}

		@Override
		public int hashCode() {
			return (17 * 31 + Float.floatToIntBits(x)) * 31
					+ Float.floatToIntBits(y);
		}
	}

	@Override
	public boolean equals(Object obj) {
		Chromosome param = (Chromosome) obj;
		boolean one = param.m_vertices_count == this.m_vertices_count;
		boolean two = param.m_wheel_count == this.m_wheel_count;
		if (one && two) {
			boolean three = true;
			for (int i = 0; i < m_wheel_count; i++) {
				boolean four = param.m_wheel_size.get(i).equals(
						this.m_wheel_size.get(i));
				boolean five = param.m_wheel_speed.get(i).equals(
						this.m_wheel_speed.get(i));
				boolean six = param.m_wheel_vertices_index.get(i).equals(
						this.m_wheel_vertices_index.get(i));
				if (!(four && five && six)) {
					three = false;
					break;
				}
			}
			for (int i = 0; i < m_vertices_count; i++) {
				boolean x = this.m_vertices.get(i).x == param.m_vertices.get(i).x;
				boolean y = this.m_vertices.get(i).x == param.m_vertices.get(i).x;
				if (!(x && y)) {
					three = false;
					break;
				}
			}
			if (three)
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int result = 17;
		for (int i = 0; i < m_wheel_count; i++)
			result = 31 * result + m_wheel_size.get(i).intValue();
		return result;
	}
}
