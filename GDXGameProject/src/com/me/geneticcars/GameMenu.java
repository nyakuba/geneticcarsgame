package com.me.geneticcars;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.me.geneticcars.menu.controller.MenuInputListener;
import com.me.geneticcars.menu.view.MainMenuScreen;

public class GameMenu {
	private ScreenChange m_game;
	private Stage m_stage;
	private GameMenuActors m_actors;
	
	public GameMenu(final ScreenChange game, final Stage st, final CarGeneration generation){
		m_game = game;
		m_stage = st;
		m_actors = new GameMenuActors();
		m_actors.addToStage(m_stage);
		m_actors.setMainMenuButtonListener(new MenuInputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				//save progress
				IOUtils xml = new IOUtils("progress.xml");
				xml.create();
				for(Chromosome chr : generation.populationChromo)
					xml.addToXML(chr);
				xml.close();
				//go to main menu
				m_game.setMenuScreen();
				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}
	
	public void draw(){
		m_stage.act(Gdx.graphics.getDeltaTime());
		m_stage.draw();
	}
	
	public void dispose(){
		m_actors.dispose();
	}
	
	public void hide(){
		m_actors.hide();
	}
	
	public void show(){
		m_actors.show();
	}
}
