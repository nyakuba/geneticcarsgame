package com.me.geneticcars;

import com.badlogic.gdx.math.Vector2;

/**
 * A convenient structure, that encapsulates some car's parameters and 
 * allows to change and mutate them. 
 */
public class CarParameters implements Cloneable{
	public Vector2 m_vertices[];
	public int m_verticesCount;
	public int m_wheelCount;
	public Vector2 m_wheelPos[];
	public float m_wheelRadius[];
	public float m_wheelSpeed[];
	
	public CarParameters(){}
	
	/**	
	 * 	@param vertices[] - relative coordinates of vertices
	 *	@param wheelCount - summary wheel count
	 *	@param wheelPos[] - relative coordinates of wheels
	 *	@param whhelRadius[] - wheel's radius
	 *	@param wheelSpeed[] wheel's speed
	 */
	public CarParameters(Vector2 vertices[], int wheelCount, Vector2 wheelPos[], float wheelRadius[], float wheelSpeed[]){
		m_vertices = vertices;
		m_wheelCount = wheelCount;
		m_wheelPos = wheelPos;
		m_wheelRadius = wheelRadius;
		m_wheelSpeed = wheelSpeed;
	}
	
	/**
	 * 	@param chr - a chromosome, used in genetic algorithm.
	 */
	public CarParameters(Chromosome chr){
		Vector2 vert[] = new Vector2[chr.m_vertices_count];
		for(int i=0; i<chr.m_vertices_count; i++)
			vert[i] = chr.m_vertices.get(i).toVector2();
		int wcount = chr.m_wheel_count;
		Vector2 wheels[] = new Vector2[wcount];
		float wradius[] = new float[wcount];
		float wspeed[] = new float[wcount];
		for(int i=0; i<wcount; i++){
			int index = chr.m_wheel_vertices_index.get(i);
			wheels[i] = chr.m_vertices.get(index).toVector2();
			wradius[i] = chr.m_wheel_size.get(i);
			wspeed[i] = chr.m_wheel_speed.get(i);
		}
		m_vertices = vert;
		m_wheelCount = wcount;
		m_wheelPos = wheels;
		m_wheelRadius = wradius;
		m_wheelSpeed = wspeed;
	}
	
	@Override
	public Object clone(){
		CarParameters result = new CarParameters();
		result.m_vertices = new Vector2[m_vertices.length];
		for(int i=0; i<m_vertices.length; i++){
			result.m_vertices[i] = m_vertices[i].cpy();
		}
		result.m_wheelPos = new Vector2[m_wheelPos.length];
		for(int i=0; i<m_wheelPos.length; i++){
			result.m_wheelPos[i] = m_wheelPos[i].cpy();
		}
		result.m_wheelCount = m_wheelCount;
		result.m_wheelRadius = m_wheelRadius.clone();
		result.m_wheelSpeed = m_wheelSpeed.clone();
		return result;
	}
	
}
