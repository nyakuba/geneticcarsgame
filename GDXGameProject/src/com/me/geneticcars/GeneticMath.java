package com.me.geneticcars;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;


/**
 * Класс для всякого вроде сортировки точек для построения тела и 
 * для всего чего только вздумается.
 * @author alexander
 *
 */
public class GeneticMath {

	    /**
	     * Сортирует мн-во точек(points) так, чтобы из них потом можно было построить тело машинки.
	     * ...смотри конструктор класса Car 
	     * @param points
	     * @return
	     */
		public static ArrayList<Vector2> japanSort(ArrayList<Vector2> points){
			ArrayList<Vector2> freePoint = new ArrayList<Vector2>(points);
			ArrayList<Vector2> sortPoint = new ArrayList<Vector2>();
			
			int size = freePoint.size();
			
			//Центр масс точек
			Vector2 massCenter = new Vector2(0, 0);
			
			//Находим центр масс и точку из набора ближашую к нему
			for(int i = 0; i < size; i++){
				massCenter.x += freePoint.get(i).x;
				massCenter.y += freePoint.get(i).y;
			}
			
			massCenter.x /= freePoint.size();
			massCenter.y /= freePoint.size();
			
			int indexNearestCenterPoint = 0;
			for(int i = 1; i < size; i++){
				if(freePoint.get(i).dst(massCenter) < freePoint.get(indexNearestCenterPoint).dst(massCenter))
					indexNearestCenterPoint = i;
			}
			
			Vector2 nearest = new Vector2(freePoint.get(indexNearestCenterPoint));
			
			sortPoint.add(nearest);
			freePoint.remove(nearest);
			
			//Берем произвольную точку из оставшихся и отбираем оставшиеся точки против часовой стрелки
			
			
			ArrayList<Vector2> buf = angleSort(sortPoint.get(0), freePoint);
			
			//Возможна ситуация, когда две точки на одной линии относительно центра.
			//В таком случае выберем наиболее далекую от центра, а ближайшую удалим
			for(int i = 1; i < buf.size()-1; i++){
				if((new Vector2(buf.get(i).x - sortPoint.get(0).x, buf.get(i).y - sortPoint.get(0).y)).angle() ==
						(new Vector2(buf.get(i-1).x - sortPoint.get(0).x, buf.get(i-1).y - sortPoint.get(0).y)).angle()){
					
					if((new Vector2(buf.get(i).x - sortPoint.get(0).x, buf.get(i).y - sortPoint.get(0).y)).len() <=
							(new Vector2(buf.get(i-1).x - sortPoint.get(0).x, buf.get(i-1).y - sortPoint.get(0).y)).len()){
						buf.remove(i);
					} else {
						buf.remove(i-1);
					}
					
					i--;
					
				}
			}
			
			for(int i = 0; i < buf.size(); i++){
				sortPoint.add(buf.get(i));
			}
						
			
			return sortPoint;
			
		}
		
		/**
		 * Сортирует массив точек по углу между вектором center->points[i]
		 * и осью координат Х  
		 * @param center
		 * @param points
		 * @return
		 */
		public static ArrayList<Vector2> angleSort(Vector2 center, ArrayList<Vector2> points){
			ArrayList<Vector2> freePoint = new ArrayList<Vector2>(points);
			ArrayList<Vector2> sortPoint = new ArrayList<Vector2>();
			
			while(freePoint.size() != 0){
				Vector2 pointMinAngle = freePoint.get(0);
				
				for(int i = 1; i < freePoint.size(); i++){
					if((new Vector2(freePoint.get(i).x - center.x, freePoint.get(i).y - center.y)).angle() <=
					(new Vector2(pointMinAngle.x - center.x, pointMinAngle.y - center.y)).angle()){
						pointMinAngle = freePoint.get(i);
					}
				}
				
				sortPoint.add(pointMinAngle.cpy());
				freePoint.remove(pointMinAngle);
			}
			
			return sortPoint;
		}
		
		
		/**!!!!!!!!!!!!!!Атавизм
		 * Подарочный алгоритм.
		 * Исключительно для лузлов ибо найдено более удобное решение проблемы
		 * Что делает: из данного мн-ва точек(points) выбирает и возвращает точки которые являются оболочкой мн-ва.
		 * @param points
		 * @return
		 */
		public static ArrayList<Vector2> giftAlgoritm(ArrayList<Vector2> points){
			final int size = points.size();
			ArrayList<Vector2> freePoint = new ArrayList<Vector2>(points);
			ArrayList<Vector2> sortPoint = new ArrayList<Vector2>();
			
			int i;
			Vector2 minP = freePoint.get(0);
			//находим левый нижний
			for(i = 0; i < size; i++){
				if((freePoint.get(i).x - minP.x + freePoint.get(i).y - minP.y) < 0)
					minP = freePoint.get(i);
			}
			//добавляем в отсортированный массив
			sortPoint.add(minP);
			i = (freePoint.indexOf(minP) + 1)%size;
			
			//найдем две опорные точки		
			Vector2 firstP;
			Vector2 secondP;
			firstP = freePoint.get(i);
			secondP = freePoint.get((i+1)%size);
			
			secondP = getVectorMaxAngle(minP, firstP, freePoint);
			firstP = secondP;
			secondP = getVectorMaxAngle(minP, firstP, freePoint);
			
			//кладем две опорные точки в отсортированный список
			
			//Да простит меня господь за этот код, но он работает
			if( firstP.y > minP.y && secondP.y > minP.y){//в 1ой четверти оба
				if(getCosOfAngle(new Vector2(1, 0), new Vector2(firstP.x - minP.x, firstP.y - minP.y)) < 
				getCosOfAngle(new Vector2(1, 0), new Vector2(secondP.x - minP.x, secondP.y - minP.y)) ){
					sortPoint.add(0, firstP);
					sortPoint.add(secondP);
				}else{
					sortPoint.add(0, secondP);
					sortPoint.add(firstP);
				}
			}
			
			if( firstP.y < minP.y && secondP.y < minP.y){//в 2ой четверти оба
				if(getCosOfAngle(new Vector2(1, 0), new Vector2(firstP.x - minP.x, firstP.y - minP.y)) > 
				getCosOfAngle(new Vector2(1, 0), new Vector2(secondP.x - minP.x, secondP.y - minP.y)) ){
					sortPoint.add(0, firstP);
					sortPoint.add(secondP);
				}else{
					sortPoint.add(0, secondP);
					sortPoint.add(firstP);
				}
			}
			
			if( firstP.y >= minP.y && secondP.y <= minP.y){
				sortPoint.add(0, firstP);
				sortPoint.add(secondP);
			}
			
			if( firstP.y <= minP.y && secondP.y >= minP.y){
				sortPoint.add(0, secondP);
				sortPoint.add(firstP);
			}
			
			//с помощью опорных точек находим оболочку
			int j = 1;
			while(j < sortPoint.size()){
				Vector2 newP = getVectorMaxAngle(sortPoint.get(j+1), sortPoint.get(j), freePoint);
				if(newP.x == sortPoint.get(0).x && newP.y == sortPoint.get(0).y)
					j = size;
				else{
					sortPoint.add(newP);
					j++;
				}
			}
	
			return sortPoint;
		}
		
		
		
		/**!!!!!!!!!!!!!!Атавизм
		 * По заданому вектору(zero -> a) из набора точек находит и
		 * возвращает точку A такую, что угол между векторами zero->a и zero->A максимальный
		 * @param zero
		 * Начало опорного вектора
		 * @param a
		 * Конец опорного вектора
		 * @param points
		 * Список проверяемых точек
		 * @return
		 */
		public static Vector2 getVectorMaxAngle(Vector2 zero, Vector2 a, ArrayList<Vector2> points){
			int size = points.size();
			int indexZero = points.indexOf(zero);
			int indexA = points.indexOf(a);
			int indexMaxAngle = (points.indexOf(a) + 1)%size; //индекс точки, дающей наибольший угол
			
			if(indexMaxAngle == indexZero)
				indexMaxAngle = (indexMaxAngle+1)%size;
			
			int indexAngle = indexMaxAngle; //индекс проверяемой точки
			
			while(indexAngle != indexA){
				if(indexAngle != indexZero){
					
					if(getCosOfAngle(new Vector2(a.x - zero.x, a.y - zero.y), 
							new Vector2(points.get(indexMaxAngle).x - zero.x, points.get(indexMaxAngle).y - zero.y)) > 
							getCosOfAngle(new Vector2(a.x - zero.x, a.y - zero.y), 
									new Vector2(points.get(indexAngle).x - zero.x, points.get(indexAngle).y - zero.y))
							){
						
						//Если косинус меньше - угол больше. 
						indexMaxAngle = indexAngle;				
					}
					
				}
				indexAngle = (indexAngle + 1)%size;
			}
			
			return points.get(indexMaxAngle);
		}
		
		
		/**!!!!!!!!!!!!!!Атавизм
		 * Возвращает косинус угла между векторами
		 * @param a
		 * @param b
		 * @return
		 */
		public static double getCosOfAngle(Vector2 a, Vector2 b){
			return (a.x*b.x + a.y*b.y)/
					(Math.pow(a.x*a.x + a.y*a.y, 1.0 / 2)*Math.pow(b.x*b.x + b.y*b.y, 1.0 / 2));
					
		}
		
		
	
}
