package com.me.geneticcars;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class GameMenuActors {
	
	private Button m_main_menu;
	private Skin m_skin;
	
	public GameMenuActors(){
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("options_menu.pack"));
		m_skin = new Skin(atlas);
		m_main_menu = new Button(m_skin.getDrawable("back"));
		m_main_menu.setPosition(500, 500);
		m_main_menu.setColor(0.7f,0.7f,0.7f,1);
	}

	public void addToStage(Stage stage){
		stage.addActor(m_main_menu);
	}
	
	public void setMainMenuButtonListener(EventListener listener){
		m_main_menu.addListener(listener);
	}
	
	public void dispose(){
		m_skin.dispose();
	}
	
	public void hide(){
		m_main_menu.setVisible(false);
	}
	
	public void show(){
		m_main_menu.setVisible(true);
	}
}
