package com.me.geneticcars;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.me.geneticcars.menu.InitParams;

public class WorldScreen<WorldController> implements Screen{

	static final float WORLD_TO_BOX = 0.01f;
	static final float BOX_TO_WORLD = 100f;
	static final float GRAVITY_X = 0f;
	static final float GRAVITY_Y = -5000f;
	static final int X_RESOLUTION = 1280;
	static final int Y_RESOLUTION = 720;
	static final int CAMERA_TRACKING_BUFFER_X = X_RESOLUTION/4;
	static final int CAMERA_TRACKING_BUFFER_Y = 20;
	static final int CAMERA_TRACKING_SPEED = 5;
	
	//for drag
	float dragX = 0;
	float dragY = 0;
	boolean dragActiv = false;
	
	private ScreenChange m_game;
	private GameMenu m_menu;
	private Stage m_stage;
	private InitParams param;
	private boolean isActive;
	private boolean isLoaded;
	private List<Chromosome> progress;
	
	public WorldScreen(final ScreenChange gam, InitParams params) {
		this.m_game = gam;
		this.param = params;
		isActive = false;
		isLoaded = false;
	}
	
	public void loadProgress(List<Chromosome> list){
		progress = list;
		isLoaded = true;
	}
	
	private void create(){
		m_ground = new PanelSet(m_world);
		m_ground.add(new Vector2(0f, 400f), 100f, 10f, -90f);
		m_ground.add(100f, 10f, -10f);
		m_ground.add(100f, 10f, 10f);
		m_ground.add(100f, 10f, -10f);
		m_ground.add(100f, 10f, 10f);
		m_ground.generateRandomPanel(param.panel_count, param.seed, null);
		// TODO: не звбыть встроить какую-нибудь проверку на конец трассы.
		m_ground.add(100f, 10f, 90f);

		///////////////////////////
		Vector2 vertE[] = { new Vector2(63,63), new Vector2(116,39), new Vector2(70,96)};
		int wheelCountE = 1;
		Vector2 wheelPosE[] = { vertE[0]};
		float radiusE[] = { 42 };
		float speedE[] = { 160 };
		///////////////////////////////////////////
		CarParameters cparamE = new CarParameters(vertE, wheelCountE, wheelPosE, radiusE, speedE);
		
		try{
			
			CarE = new Car(cparamE);
			CarE.create(m_world);
			
		} catch (Exception ex){
			System.err.println(ex);
		}
		
		m_touchIsActive = false;
		m_touchPos = new Vector3(0,0,0);
		m_touchFixture = CarE.getBodyFixture();
		
		m_leader = CarE;
		
		
		Random gen = new Random();
		CarE.dispose();
		if(!isLoaded){
			bubn = new CarGeneration(m_world, gen, param.mutation_rate, param.car_count);
		} else if(progress != null){
			bubn = new CarGeneration(m_world, gen, param.mutation_rate, progress);
			isLoaded = false;
		}
		
		
		bubn.initSelection();

		m_menu = new GameMenu(m_game, m_stage, bubn);
	}

	@Override
	public void dispose() {
		for(Car c : m_cars){
			c.dispose();
		}
		m_ground.dispose();
		m_stage.dispose();
		m_menu.dispose();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		

		
		m_camera.update();

		// Box2D Debug Renderer
		m_debugRenderer.render(m_world, m_camera.combined);
		
		m_menu.draw();
		
		bubn.selection();
		// Step Box2D simulations
		m_world.step(1 / 300f, 6, 2);
		
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void show() {
		if(!isActive){
			m_stage = new Stage(){
				// TODO: аккуратно, на Stage еще навешана обработка кликов. То, что я доопределяю здесь методы совсем не хорошо. 
				@Override
				public boolean touchUp(int screenX, int screenY, int pointer,
						int button) {
					dragActiv = false;
					return false;
				}
				@Override
				public boolean touchDragged(int screenX, int screenY, int pointer) {
					if(dragActiv){
						
						m_camera.translate((dragX - m_camera.position.x - (float)screenX), -(dragY - m_camera.position.y - (float)screenY));
						
						dragX = screenX + m_camera.position.x;
						dragY = screenY + m_camera.position.y;
					}else{
						dragActiv = true;
						dragX = screenX + m_camera.position.x;
						dragY = screenY + m_camera.position.y;
					}
					return false;
				}
				@Override
				public boolean scrolled(int amount) {
							
					m_camera.zoom += ((float)amount)/20;
					if(m_camera.zoom < 0)
						m_camera.zoom = 0.01f;
					
					return false;
				}
			};
			m_world = new World(new Vector2(GRAVITY_X, GRAVITY_Y), true);
			
			m_camera = new OrthographicCamera();
			m_camera.setToOrtho(false, X_RESOLUTION*2, Y_RESOLUTION*2);
			m_world = new World(new Vector2(GRAVITY_X, GRAVITY_Y), true);
			m_debugRenderer = new Box2DDebugRenderer(true,true,false,true,false);
			
			Gdx.input.setInputProcessor(m_stage);
			this.create();
			isActive = true;
		}
	}

	@Override
	public void hide() {
		if(isActive){
			m_stage.dispose();
			m_menu.dispose();
			m_world.dispose();
			m_stage = null;
			m_menu = null;
			m_world = null;
			isActive = false;
		}
	}
	
	private OrthographicCamera m_camera;
	private World m_world;
	private Box2DDebugRenderer m_debugRenderer;

	private Car m_cars[];
	private Car CarE;
	private PanelSet m_ground;
	
	private CarGeneration bubn;
	private Car m_leader;
	private boolean m_touchIsActive;
	private Vector3 m_touchPos;
	private Fixture m_touchFixture;
	
}
