package com.me.geneticcars;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Класс для проведения эксперементов(чтоб не засорять GameScreen)
 * Даже не читай. 
 * @author alexander
 *
 */
public class MyLabClass {
	
	public static void labGiftAlg(World m_world){
		Vector2 E_position = new Vector2(150, 150);
		Body E_body;
		BodyDef E_bodyDef;
		PolygonShape E_shape;
		Vector2 E_vertices[];
		Fixture E_fixture;
		FixtureDef E_fixtureDef;
		

		/*E_vertices[0] = new Vector2(0, 0);
		E_vertices[1] = new Vector2(40, -40);
		E_vertices[2] = new Vector2(60, 0);
		E_vertices[3] = new Vector2(80, 0);
		E_vertices[4] = new Vector2(40, 40);*/
		
		ArrayList<Vector2> sortPoint = new ArrayList<Vector2>();
		ArrayList<Vector2> giftPoint;
		
		/*sortPoint.add(new Vector2(0, 0));
		sortPoint.add(new Vector2(80, 0));
		sortPoint.add(new Vector2(80, 80));
		sortPoint.add(new Vector2(40, 40));
		sortPoint.add(new Vector2(0, 80));*/
		
		
		/*sortPoint.add(new Vector2(0, 0));
		sortPoint.add(new Vector2(-40, 20));
		sortPoint.add(new Vector2(-40, 60));
		sortPoint.add(new Vector2(0, 80));
		sortPoint.add(new Vector2(0, 40));
		sortPoint.add(new Vector2(40, 60));
		sortPoint.add(new Vector2(40, 20));*/
		
		sortPoint.add(new Vector2((float)Math.random()*50 + 10, (float)Math.random()*50 + 10));
		sortPoint.add(new Vector2((float)Math.random()*50 + 10, (float)Math.random()*50 + 10));
		sortPoint.add(new Vector2((float)Math.random()*50 + 10, (float)Math.random()*50 + 10));
		sortPoint.add(new Vector2((float)Math.random()*50 + 10, (float)Math.random()*50 + 10));
		sortPoint.add(new Vector2((float)Math.random()*50 + 10, (float)Math.random()*50 + 10));
		sortPoint.add(new Vector2((float)Math.random()*50 + 10, (float)Math.random()*50 + 10));
		sortPoint.add(new Vector2((float)Math.random()*50 + 10, (float)Math.random()*50 + 10));
		sortPoint.add(new Vector2((float)Math.random()*50 + 10, (float)Math.random()*50 + 10));
		sortPoint.add(new Vector2((float)Math.random()*50 + 10, (float)Math.random()*50 + 10));
		
		
		
		giftPoint = GeneticMath.giftAlgoritm(sortPoint);
		
		E_vertices = new Vector2[sortPoint.size()];	
		for(int k = 0; k<sortPoint.size(); k++)
			E_vertices[k] = sortPoint.get(k);
		
		/*giftPoint = new ArrayList<Vector2>();
		giftPoint.add(new Vector2(0, 0));
		giftPoint.add(new Vector2(80, 0));
		giftPoint.add(new Vector2(0, 80));*/
		
		//giftPoint.add(geneticMath.getVectorMaxAngle(sortPoint.get(0), sortPoint.get(1), sortPoint));
		
		/*E_vertices = new Vector2[giftPoint.size()];	
		for(int k = 0; k<giftPoint.size(); k++)
			E_vertices[k] = giftPoint.get(k);*/

		E_vertices = new Vector2[giftPoint.size()];	
		for(int k = 0; k<giftPoint.size(); k++)
			E_vertices[k] = giftPoint.get(k);
				
		/*if(true){
			E_vertices = new Vector2[sortPoint.size()];	
			for(int k = 0; k<sortPoint.size(); k++)
				E_vertices[k] = sortPoint.get(k);
		}else{
			E_vertices = new Vector2[giftPoint.size()];	
			for(int k = 0; k<giftPoint.size(); k++)
				E_vertices[k] = giftPoint.get(k);
		}*/

		//create body of a car
		E_bodyDef = new BodyDef();
		E_bodyDef.type = BodyType.DynamicBody;
		E_bodyDef.position.set(E_position);
		E_body = m_world.createBody(E_bodyDef);
		//create shape
		E_shape = new PolygonShape();
		E_shape.set(E_vertices);
		
		E_fixtureDef = new FixtureDef();
		E_fixtureDef.shape = E_shape;
		E_fixtureDef.density = 1;
		E_fixtureDef.friction = (float)0.1;
		E_fixtureDef.restitution = (float)0.1;
		E_fixture = E_body.createFixture(E_fixtureDef);
	}
	
	public static void labCombBody(World m_world){
		
		Vector2 startPos = new Vector2(300, 300);
		
		Vector2 E_vertices1[];
		E_vertices1 = new Vector2 [4];
		
		E_vertices1[0] = new Vector2(0, 0);
		E_vertices1[1] = new Vector2(40, 0);
		E_vertices1[2] = new Vector2(40, 40);
		E_vertices1[3] = new Vector2(0, 40);
		
		Vector2 E_vertices2[];
		E_vertices2 = new Vector2 [4];
		
		E_vertices2[0] = new Vector2(10, 0);
		E_vertices2[1] = new Vector2(10, 40);
		E_vertices2[2] = new Vector2(-30, 40);
		E_vertices2[3] = new Vector2(-30, 0);
		
		/*E_vertices2[0] = new Vector2(0, 0);
		E_vertices2[0] = new Vector2(40, 0);
		E_vertices2[0] = new Vector2(40, 40);
		E_vertices2[0] = new Vector2(0, 40);*/
		
		BodyDef bd = new BodyDef();
		bd.position.set(startPos);
		bd.type = BodyType.DynamicBody;
		Body body = m_world.createBody(bd);
		
		PolygonShape shape1 = new PolygonShape();
		shape1.set(E_vertices1);
		
		PolygonShape shape2 = new PolygonShape();
		shape2.set(E_vertices2);
		
		FixtureDef fd;
		Fixture f1;
		Fixture f2;
		
		/*fd = new FixtureDef();
		fd.shape = shape1;
		fd.density = 1;
		fd.friction = (float)0.1;
		fd.restitution = (float)0.1;	
		
		f1 = body.createFixture(fd);
		
		fd.shape = shape2;
		f2 = body.createFixture(fd);*/
		
		/*f1 = body.createFixture(shape1, 0);
		f2 = body.createFixture(shape2, 0);*/
		
		fd = new FixtureDef();
		fd.shape = shape1;
		fd.density = 1;
		fd.friction = (float)0.1;
		fd.restitution = (float)0.1;	
		
		f1 = body.createFixture(fd);
		
		fd.shape = shape2;
		
		f2 = body.createFixture(fd);
		
		/*Fixture f1 = body.createFixture(shape1, (float)1);
		// можно явно задавать fixture.
		FixtureDef fd = new FixtureDef();
		fd.density = 1;
		// etc
		Fixture f2 = body.createFixture(fd);*/
	
	}
	
	public static void labJapanAlg(World m_world){
		
		Vector2 startPos = new Vector2(100 + (float)Math.random()*1000, 100 + (float)Math.random()*1000);
		BodyDef bd = new BodyDef();
		bd.position.set(startPos);
		bd.type = BodyType.DynamicBody;
		Body body = m_world.createBody(bd);
		
		FixtureDef fd;
		fd = new FixtureDef();
		fd.density = 1;
		fd.friction = (float)0.1;
		fd.restitution = (float)0.1;
		
		Filter filter = new Filter();
		filter.maskBits = 0x0001;
		filter.categoryBits = 0x0002;
		

		
		/////+++++/////
		/////+++++/////
		
		ArrayList<Vector2> startPoint = new ArrayList<Vector2>();
		ArrayList<Vector2> japanPoint;
		
		/*startPoint.add(new Vector2(0, 0));
		startPoint.add(new Vector2(80, 0));
		startPoint.add(new Vector2(80, 80));
		startPoint.add(new Vector2(40, 40));
		startPoint.add(new Vector2(0, 80));
		startPoint.add(new Vector2(-10, 70));*/
		
		/*startPoint.add(new Vector2(0, 0));
		startPoint.add(new Vector2(-40, 20));
		startPoint.add(new Vector2(-40, 60));
		startPoint.add(new Vector2(0, 80));
		startPoint.add(new Vector2(0, 40));
		startPoint.add(new Vector2(40, 60));
		startPoint.add(new Vector2(40, 20));*/
		
		startPoint.add(new Vector2((float)Math.random()*100 + 10, (float)Math.random()*100 + 10));
		startPoint.add(new Vector2((float)Math.random()*100 + 10, (float)Math.random()*100 + 10));
		startPoint.add(new Vector2((float)Math.random()*100 + 10, (float)Math.random()*100 + 10));
		startPoint.add(new Vector2((float)Math.random()*100 + 10, (float)Math.random()*100 + 10));
		startPoint.add(new Vector2((float)Math.random()*100 + 10, (float)Math.random()*100 + 10));
		startPoint.add(new Vector2((float)Math.random()*100 + 10, (float)Math.random()*100 + 10));
		startPoint.add(new Vector2((float)Math.random()*100 + 10, (float)Math.random()*100 + 10));
		startPoint.add(new Vector2((float)Math.random()*100 + 10, (float)Math.random()*100 + 10));
		startPoint.add(new Vector2((float)Math.random()*100 + 10, (float)Math.random()*100 + 10));
		
		/*startPoint.add(new Vector2(0, 0));
		startPoint.add(new Vector2(-60, 200));
		startPoint.add(new Vector2(-40, 180));
		startPoint.add(new Vector2(-20, 240));
		startPoint.add(new Vector2(0, 200));
		startPoint.add(new Vector2(20, 240));
		startPoint.add(new Vector2(40, 180));
		startPoint.add(new Vector2(60, 200));*/
		
		
		japanPoint = GeneticMath.japanSort(startPoint);
				
		//набор треугольников	
		ArrayList<Vector2[]> Trig = new ArrayList<Vector2[]>();
		
		for(int i = 1; i < japanPoint.size()-1; i++){
			if((new Vector2(japanPoint.get(i+1).x - japanPoint.get(0).x, japanPoint.get(i+1).y - japanPoint.get(0).y)).angle() -
					(new Vector2(japanPoint.get(i).x - japanPoint.get(0).x, japanPoint.get(i).y - japanPoint.get(0).y).angle()) 
					< (float)180
					
			){
				Trig.add(new Vector2[3]);
				Trig.get(Trig.size() - 1)[0] = japanPoint.get(0);
				Trig.get(Trig.size() - 1)[1] = japanPoint.get(i);
				Trig.get(Trig.size() - 1)[2] = japanPoint.get(i+1);
			}
		}
		
		//Нет, это не дублирование кода. Шаг между точками - два, в отличии от случая с циклами.
		//Да, это костыль. Но, вроде, безобидный. 
		if((new Vector2(japanPoint.get(1).x - japanPoint.get(0).x, japanPoint.get(1).y - japanPoint.get(0).y)).angle() -
				(new Vector2(japanPoint.get(japanPoint.size() - 1).x - japanPoint.get(0).x, japanPoint.get(japanPoint.size() - 1).y - japanPoint.get(0).y).angle()) 
				> (float)180
				
		){
			Trig.add(new Vector2[3]);
			Trig.get(Trig.size() - 1)[0] = japanPoint.get(0);
			Trig.get(Trig.size() - 1)[1] = japanPoint.get(japanPoint.size() - 1);
			Trig.get(Trig.size() - 1)[2] = japanPoint.get(1);
		}
		
		
		Fixture fx[] = new Fixture[japanPoint.size()-1];
		
		for(int i = 0; i < Trig.size(); i++){
			PolygonShape shape = new PolygonShape();
			shape.set(Trig.get(i));	
			fd.shape = shape;	
			fx[i] = body.createFixture(fd);
			fx[i].setFilterData(filter);
		}
		//fd.shape = shape1;
		
		/*for(int i = 0; i < japanPoint.size(); i++)
			myLabClass.labGiftAlg(m_world);*/
		
		//if(Trig.size() == 1)
		//	myLabClass.labGiftAlg(m_world);
		
		/*if(japanPoint.size() == 5)
					myLabClass.labGiftAlg(m_world);*/
		
		
	}

}
