package com.me.geneticcars;

public interface ScreenChange {
	public void setGameScreen();
	public void loadAndSetGameScreen();
	public void setMenuScreen();
}
